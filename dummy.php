<?php
require_once(__DIR__ ."/api/class/Console.php");
require_once(__DIR__ ."/api/config/Database.php");
require_once(__DIR__ ."/api/config/core.php");
$KEYS = new Keys();

if (php_sapi_name() === 'cli') {
  // is command interface
  if ($argc != 2) {
    echo "Usage: php ". basename(__FILE__) ." <root_key>\n";
    exit();
  }
  $root_key = $argv[1];
} else if (!empty($_GET['key'])) {
  // is web
  $root_key = $_GET['key'];
} else {
  echo "provide a key";
}

// check if key provided if the root key
if ($root_key !== $KEYS->ROOT_KEY) {
  exit();
}

$db = new Database();
$conn = $db->connect();
$console = new Console();



// insert customers
$console->task("Creating customers");
$customer_query = "INSERT INTO Customers( fname, mname, lname, email, age, gender, address, phone_no, started_on, password )
  VALUES ( :firstname, :middlename, :lastname, :email, :age, :gender, :address, :phone_no, :started_on, :password )";
$customer_stmt = $conn->prepare($customer_query);
$customers = array(
  array("Beverly", "D", "Orndorff", "lizzie_kutc3@gmail.com", "20", "f", "3177 Millbrook Road\nGeneva, Illinois(IL), 60134", "7082301657", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jeff", "J", "Rodriquez", "willa1989@yahoo.com", "49", "m", "27 Watson Street\nHopewell (Mercer), New Jersey(NJ), 08525", "8622000739", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Robert", "K", "Spaulding", "dylan1999@yahoo.com", "46", "m", "4978 Overlook Drive\nWinchester, Indiana(IN), 47394", "3173674680", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ann", "P", "Holmberg", "aliyah.mcgly@yahoo.com", "33", "f", "3207 Dog Hill Lane\nTopeka, Kansas(KS), 66608", "7854098449", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Anthony", "B", "Mills", "edyth_harri6@gmail.com", "22", "m", "2630 Rainy Day Drive\nRoxbury, Massachusetts(MA), 02119", "6174588882", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Georgia", "D", "Howze", "jeffrey2011@hotmail.com", "49", "f", "4178 Old Dear Lane\nPoughkeepsie, New York(NY), 12601", "8452199418", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Deborah", "M", "Meeks", "ezequiel.polli@yahoo.com", "57", "f", "2288 Dogwood Road\nPhoenix, Arizona(AZ), 85034", "6029138984", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("John", "A", "Cooper", "ansel_shanah@yahoo.com", "20", "m", "3001 Marie Street\nBaltimore, Maryland(MD), 21202", "4436537758", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Patrick", "L", "Brady", "jeffery1984@hotmail.com", "45", "m", "839 Ocala Street\nKissimmee, Florida(FL), 34746", "3217466833", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ian", "R", "Harris", "aileen.grim@hotmail.com", "34", "m", "4651 Morris Street\nVictoria, Texas(TX), 77901", "3616499907", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Concetta", "E", "Weber", "newton2013@gmail.com", "28", "f", "96 Pinchelone Street\nNorfolk, Virginia(VA), 23505", "7573779141", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("David", "D", "Anderson", "alexzander_he@hotmail.com", "50", "m", "459 Harper Street\nHazel, Kentucky(KY), 42049", "5022025386", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Robert", "M", "Reich", "kay1973@gmail.com", "26", "m", "809 Leo Street\nBroomfield, Colorado(CO), 80020", "7203201334", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Lewis", "G", "Croteau", "elizabeth.bos@gmail.com", "41", "m", "2361 Dovetail Drive\nLombard, Illinois(IL), 60148", "3315516576", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Lisa", "B", "Sands", "llewellyn1987@yahoo.com", "46", "f", "2299 Timbercrest Road\nKodiak, Alaska(AK), 99615", "9075289984", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Aaron", "M", "Douglas", "shemar1984@gmail.com", "27", "m", "3836 Stout Street\nShippensburg, Pennsylvania(PA), 17257", "4128747356", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Suzanne", "E", "Barton", "sammie_schamberg@gmail.com", "38", "f", "1874 Victoria Court\nRome, Maine(ME), 04955", "2074172645", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Earl", "A", "Gable", "rogelio1979@gmail.com", "39", "m", "478 White Avenue\nAlice, Texas(TX), 78332", "3614605889", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Shelia", "D", "Hollingsworth", "harmon_bat7@gmail.com", "19", "f", "615 Clover Drive\nColorado Springs, Colorado(CO), 80918", "7193734436", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jennifer", "P", "Salinas", "susie_vandervo@gmail.com", "48", "f", "783 New Street\nCoos Bay, Oregon(OR), 97420", "5412973844", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Collin", "L", "Watson", "alejandra1971@gmail.com", "39", "m", "613 Florence Street\nSulphur Springs, Texas(TX), 75482", "9032436595", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Glenn", "L", "Lee", "albin_olso3@hotmail.com", "41", "m", "67 Irving Road\nRushville, Ohio(OH), 43150", "4408228395", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Geoffrey", "J", "Wilcher", "tara.torph0@gmail.com", "45", "m", "3606 Kimberly Way\nKentwood, Michigan(MI), 49512", "2692831863", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jennifer", "Z", "Ford", "gilberto2013@hotmail.com", "26", "f", "2642 Peaceful Lane\nGarfield Heights, Ohio(OH), 44125", "6147120330", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Larry", "J", "Perkins", "domenick1991@gmail.com", "30", "m", "4215 Langtown Road\nFindlay, Ohio(OH), 45840", "4197885569", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Christina", "R", "Jarrett", "maribel_wilkins@yahoo.com", "28", "f", "3972 Liberty Street\nDallas, Texas(TX), 75202", "2146698499", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Tosha", "R", "Monarrez", "brionna1986@hotmail.com", "43", "f", "38 Eagle Street\nCentralia, Illinois(IL), 62801", "7739418395", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Shawn", "J", "Heard", "diego1989@gmail.com", "33", "m", "4321 Flinderation Road\nBlue Island, Illinois(IL), 60406", "7082591274", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Sara", "T", "Paige", "bo2004@gmail.com", "27", "f", "1755 Aspen Court\nNewton, Massachusetts(MA), 02160", "6175469992", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jean", "R", "Harrington", "mariano.cartwrig@gmail.com", "55", "f", "914 Marietta Street\nConcord, California(CA), 94520", "6269757018", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Edward", "B", "Barto", "rolando2010@gmail.com", "34", "m", "2295 Lena Lane\nHattiesburg, Mississippi(MS), 39401", "7692239884", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Claude", "A", "Webb", "may_bode1979@yahoo.com", "27", "m", "2132 Bee Street\nHesperia, Michigan(MI), 49421", "7344912683", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ronald", "E", "Howard", "marilie.steub@hotmail.com", "42", "m", "1677 Todds Lane\nSan Antonio, Texas(TX), 78213", "2102878927", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Audrey", "C", "Sartin", "alexzande10@yahoo.com", "56", "f", "3870 Mattson Street\nPortland, Oregon(OR), 97205", "5033107371", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("William", "M", "Salzman", "ryleigh_dubuq@gmail.com", "37", "m", "3551 Bassel Street\nMetairie, Louisiana(LA), 70001", "3372415317", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Franklin", "C", "Walton", "lazaro_roo4@gmail.com", "54", "m", "1831 Hill Street\nPort Clinton, Ohio(OH), 43452", "6144505995", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Joan", "G", "Smith", "mona1971@hotmail.com", "51", "f", "1231 Freed Drive\nStockton, California(CA), 95204", "3102004342", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Michael", "P", "Robertson", "clovis1976@hotmail.com", "64", "m", "3112 Trainer Avenue\nTopeka, Illinois(IL), 61567", "7082962788", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jorge", "M", "Sturgis", "shanon.dickins@gmail.com", "58", "m", "3405 Cerullo Road\nLouisville, Kentucky(KY), 40202", "5026902489", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ollie", "J", "Hazard", "glenna1979@hotmail.com", "26", "f", "828 Meadowview Drive\nFredericksburg, Virginia(VA), 22408", "7575929291", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Cheryl", "M", "Jackson", "abel.hilper9@yahoo.com", "29", "f", "1469 Nickel Road\nPasadena, California(CA), 91101", "6263193591", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Kelli", "E", "Knox", "ethelyn_schup@yahoo.com", "58", "f", "2929 Oxford Court\nColumbus, Mississippi(MS), 39701", "6624259930", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Brittany", "D", "Meyers", "pearline_mitche@hotmail.com", "42", "f", "4009 Valley Lane\nAustin, Texas(TX), 78746", "5122984722", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Sergio", "V", "Craig", "emmie.cai3@gmail.com", "36", "m", "1108 Public Works Drive\nBluff City, Tennessee(TN), 37618", "6155851547", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Robert", "D", "Young", "caden_mccullou@gmail.com", "35", "m", "4481 Del Dew Drive\nOxon Hill, Maryland(MD), 20021", "4432016122", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Janice", "W", "Maciejewski", "kitty_erdma5@yahoo.com", "49", "f", "2206 Joseph Street\nNew Berlin, Wisconsin(WI), 53151", "4145198166", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Emily", "R", "Stevens", "jadyn_hue8@gmail.com", "43", "f", "2450 Radio Park Drive\nAugusta, Georgia(GA), 30906", "7063997877", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Delores", "J", "Atherton", "delfina2002@hotmail.com", "54", "f", "3791 Loving Acres Road\nKansas City, Missouri(MO), 64137", "8162699211", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Brooke", "R", "Sherman", "miouri1990@yahoo.com", "28", "f", "3968 Passaic Street\nWashington, Washington DC(DC), 20036", "5623189092", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Virginia", "B", "Eberhardt", "zechariah1985@yahoo.com", "38", "f", "946 Terry Lane\nOrlando, Florida(FL), 32801", "4078793972", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Geneva", "B", "Rollin", "german_murp@gmail.com", "59", "f", "637 Adamsville Road\nLaredo, Texas(TX), 78040", "9569495481", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Robert", "K", "Harris", "dana.homeni@hotmail.com", "48", "m", "3776 Irving Road\nSaint Clairsville, Ohio(OH), 43950", "7402963156", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ronald", "W", "Elkins", "sydni1982@hotmail.com", "55", "m", "1557 Seneca Drive\nPortland, Oregon(OR), 97205", "9712012085", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Terry", "J", "Kinney", "cleo_boeh2@yahoo.com", "40", "m", "4600 Wood Duck Drive\nRepublic, Michigan(MI), 49879", "9062822100", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Manuela", "R", "Carrillo", "lacey19881983@gmail.com", "46", "f", "2112 Lightning Point Drive\nMemphis, Tennessee(TN), 38110", "9013381495", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Elizabeth", "D", "Harrington", "melvina1996@hotmail.com", "58", "f", "2882 Pheasant Ridge Road\nPhiladelphia, Pennsylvania(PA), 19146", "2672261734", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Homer", "S", "Flores", "miguel1974@yahoo.com", "47", "m", "3013 Mulberry Avenue\nHeber Springs, Arkansas(AR), 72543", "4792006000", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Cristobal", "J", "Wainwright", "preston_kel@yahoo.com", "56", "m", "3503 Cost Avenue\nBeltsville, Maryland(MD), 20705", "2406068043", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Richard", "F", "Eldred", "adaline.bog@hotmail.com", "44", "m", "4054 Pike Street\nSan Diego, California(CA), 92128", "6197504417", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Patricia", "J", "Morris", "savion1994@yahoo.com", "67", "f", "1086 Koontz Lane\nPacoima, California(CA), 91331", "8187911000", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Christine", "G", "Mayer", "brigitte1989@gmail.com", "45", "f", "4495 Circle Drive\nHouston, Texas(TX), 77020", "8324969070", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jerry", "L", "Eisner", "carolyn2011@hotmail.com", "35", "m", "2854 Duke Lane\nSayreville, New Jersey(NJ), 08879", "5512002716", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("May", "R", "Goins", "bella_schust@gmail.com", "50", "f", "4393 Woodland Avenue\nMetairie, Louisiana(LA), 70001", "32646", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Inez", "C", "Moore", "bo2006@gmail.com", "26", "f", "526 Laurel Lee\nEagan, Minnesota(MN), 55121", "6122312607", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Keith", "L", "Harris", "ferne2004@gmail.com", "40", "m", "2273 Prospect Valley Road\nBurbank, California(CA), 91505", "6262307263", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Wayne", "E", "Hess", "garnet_goodw@yahoo.com", "42", "m", "1877 Washburn Street\nBaton Rouge, Louisiana(LA), 70814", "2252247732", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Helen", "W", "Dixon", "emerson1979@hotmail.com", "51", "f", "4150 Spinnaker Lane\nLisle, Illinois(IL), 60532", "7082005748", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Heather", "D", "Bradford", "heather_oconn@hotmail.com", "25", "f", "2614 Mayo Street\nLexington, Kentucky(KY), 40505", "5027270925", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Deborah", "T", "Spencer", "corbin1994@yahoo.com", "55", "f", "411 Oak Lane\nSedalia, Missouri(MO), 65301", "6602216096", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Pamela", "R", "Vinson", "leanna.hell@gmail.com", "34", "f", "683 Gnatty Creek Road\nFarmingdale, New York(NY), 11735", "5164738936", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Lorinda", "T", "Cleaver", "eddie.frits@gmail.com", "30", "f", "1902 Lilac Lane\nSapelo Island, Georgia(GA), 31327", "9128505557", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Betty", "D", "Hickey", "orrin2015@yahoo.com", "44", "f", "3713 Joy Lane\nLos Angeles, California(CA), 90071", "3109958576", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Christy", "J", "Campbell", "alexandre1974@yahoo.com", "28", "f", "1441 Overlook Drive\nIndianapolis, Indiana(IN), 46225", "3177172850", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Penelope", "P", "Hui", "magnolia.ja@hotmail.com", "40", "f", "4156 Bottom Lane\nTonawanda, New York(NY), 14150", "7164712505", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Peggy", "C", "Weber", "wilmer.labad@hotmail.com", "36", "f", "1150 Elm Drive\nGarden City, New York(NY), 11530", "5164565826", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jeffery", "F", "Nelson", "bernhard1982@hotmail.com", "31", "m", "1504 Grasselli Street\nManchester, New Hampshire(NH), 03101", "6034921629", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Anna", "J", "White", "michaela1986@hotmail.com", "36", "f", "1485 Drainer Avenue\nTallahassee, Florida(FL), 32301", "3528070342", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Antonio", "M", "Gonzalez", "dock_dibber4@gmail.com", "35", "m", "85 Wright Court\nVestavia Hills, Alabama(AL), 35216", "3343816873", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ryan", "V", "Dowdy", "damion_boeh9@hotmail.com", "63", "m", "1617 Hillside Drive\nLafayette, Louisiana(LA), 70501", "3377333283", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Ruth", "B", "Bates", "guiseppe.bog@gmail.com", "41", "f", "828 Heavens Way\nFort Myers, Florida(FL), 33912", "3523961437", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jacqueline", "H", "Joseph", "lera2015@yahoo.com", "56", "f", "21 Walnut Avenue\nMeridian, Idaho(ID), 83642", "2084124692", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Kathleen", "J", "Salazar", "irving1981@gmail.com", "35", "f", "3598 Geraldine Lane\nNew York, New York(NY), 10013", "6462007065", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Gregory", "P", "Snyder", "eldora_kuph@hotmail.com", "25", "m", "3218 Kessla Way\nConway, South Carolina(SC), 29528", "8434652064", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("William", "N", "Meehan", "osborne2016@gmail.com", "36", "m", "2936 Fairway Drive\nSan Jose, California(CA), 95113", "4088385732", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("John", "B", "Buendia", "bridget2007@gmail.com", "46", "m", "2247 Lynn Avenue\nWausau, Wisconsin(WI), 54401", "7155815407", password_hash("princeofpersia", PASSWORD_BCRYPT))
);
$customer_num = 0;
foreach ($customers as $customer) {
  if (create_customer($console, $customer_stmt, $customer[0], $customer[1], $customer[2], $customer[3], $customer[4], $customer[5], $customer[6], $customer[7], $customer[8])) {
    $customer_num++;
  }
}
$console->print("Created $customer_num customers\n");

// insert staffs
$console->task("Creating staff");
$staff_query = "INSERT INTO Staffs ( fname, mname, lname, email, age, role, gender, phone_no, started_on, salary, password )
  VALUES ( :firstname, :middlename, :lastname, :email, :age, :role, :gender, :phone_no, :started_on, :salary, :password )";
$staff_stmt = $conn->prepare($staff_query);
$staffs = array(
  array("Teresa", "W", "Gillmore", "felipa1992@hotmail.com", "43", "staff", "f", "7147248062", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jason", "A", "Jackson", "winona.johnst@hotmail.com", "64", "staff", "m", "2563286529", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Betty", "W", "Brown", "tyra_christians@hotmail.com", "63", "staff", "f", "8188143852", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Amy", "G", "Simpson", "hailey2010@yahoo.com", "35", "staff", "f", "4232086827", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Mary", "M", "Sherman", "bernie1989@hotmail.com", "40", "staff", "f", "9174198609", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Roger", "I", "Jones", "coy2010@yahoo.com", "61", "staff", "m", "7742621700", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Danny", "T", "Bibler", "cyrus_reill10@yahoo.com", "30", "staff", "m", "6464982422", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Dan", "A", "Low", "weston_kaul@hotmail.com", "37", "staff", "m", "5102241569", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Angela", "C", "Benton", "raphael1970@gmail.com", "46", "staff", "f", "9378028993", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Anthony", "F", "Rheaume", "michel_hirt@gmail.com", "55", "staff", "m", "3479284777", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Christine", "M", "Holley", "jayden.ra1@gmail.com", "41", "staff", "f", "3054967984", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Gregory", "M", "Butler", "myrtis2001@gmail.com", "33", "staff", "m", "2062935415", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Katie", "R", "Davis", "fanny1989@yahoo.com", "62", "staff", "f", "3606701574", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Andrew", "H", "Pegg", "audie2017@hotmail.com", "52", "staff", "m", "8017878451", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Stanley", "A", "Thomas", "jacinthe_ledn@yahoo.com", "48", "staff", "m", "3179897131", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Perry", "D", "Oakes", "loren1981@gmail.com", "38", "staff", "m", "5418154095", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Nathaniel", "A", "Easley", "emiliano1970@hotmail.com", "28", "staff", "m", "9794792933", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Eugene", "T", "Reid", "antonette.kie@gmail.com", "45", "staff", "m", "8087218255", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Xavier", "K", "Worthington", "jazmyne1986@hotmail.com", "37", "staff", "m", "4054824776", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Carol", "S", "Lofton", "gilbert.berni@gmail.com", "34", "staff", "f", "44512", password_hash("princeofpersia", PASSWORD_BCRYPT))
);

$staff_num = 0;
foreach ($staffs as $staff) {
  if (create_staff($console, $staff_stmt, $staff[0], $staff[1], $staff[2], $staff[3], $staff[4], $staff[5], $staff[6], $staff[7], $staff[8])) {
    $staff_num++;
  }
}
$console->print("Created $staff_num staffs\n");

// insert admins
$console->task("Creating admins");
$admin_query = "INSERT INTO Staffs ( fname, mname, lname, email, age, role, gender, phone_no, started_on, salary, password )
  VALUES ( :firstname, :middlename, :lastname, :email, :age, :role, :gender, :phone_no, :started_on, :salary, :password )";
$admin_stmt = $conn->prepare($admin_query);
$admins = array(
  array("Gregory", "M", "White", "esteban1972@gmail.com", "42", "admin", "m", "2017742156", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Sarah", "G", "Navarro", "herbert1995@yahoo.com", "49", "admin", "f", "7192002272", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Jeffrey", "C", "Kinley", "merl2000@yahoo.com", "44", "admin", "m", "3019968354", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("Victoria", "J", "Thatcher", "johnson.jaskols@gmail.com", "40", "admin", "f", "5734145987", password_hash("princeofpersia", PASSWORD_BCRYPT)),
  array("George", "M", "Alston", "dayna_jerd6@yahoo.com", "40", "admin", "m", "6197728148", password_hash("princeofpersia", PASSWORD_BCRYPT))
);

$admin_num = 0;
foreach ($admins as $admin) {
  if (create_staff($console, $admin_stmt, $admin[0], $admin[1], $admin[2], $admin[3], $admin[4], $admin[5], $admin[6], $admin[7], $admin[8])) {
    $admin_num++;
  }
}
$console->print("Created $admin_num admins\n");

// insert services
$console->task("Creating services");
$service_query = "INSERT INTO Services (name, description, price, time_req )
  VALUES (:name, :description, :price, :time_req)";
$service_stmt = $conn->prepare($service_query);
$services = array( 
 array("Aromatherapy shower", 1200, "Wake up your hair and senses in one step. The Tea Tree Special Line is fragrance with a purpose.", 60*60),
 array("Hair Cutting", 250, "Cut and trim your hair, make a new you and define yourself.", 20*60),
 array("Hair Coloring", 1500, "Get your tresses ready for some beautiful shades with colour stripping", 45*60),
 array("Hair styling",1500, "Straighten or crimp your hair to get a sleek, frizz-free and stylish hairstyle", 3*60*60),
 array("Facial And Bleach", 800, "Looking to cover dark patches or hide unpleasant facial hair? Lighten and irighten your skin by availing our bleaching services ", 2*60*60),
 array("Golden Facial", 3000, "Golden facial revitalizes your face and instantly gives you that luminous and radiant complexion", 2*60*60),
 array("Waxing", 600, "Our fast and effective waxing services will leave your skin hair-free and fabulously smooth ", 45*60),
 array("Scalp Treatment", 1800, "Scalp treatment will stimulate blood circulation and nourish your roots", 3*60*60),
 array("Eyebrow And Eyelash",500,"Enhance your eyebrow shape and create the eyebrows you've always wanted", 45*60),
 array("Beard Styling",300,"Beards are both smart and distinct if you understand how to grow and style them", 45*60),
 array("Nail Art", 600, "Nail art is a creative way to paint,decorate and embellish the nails", 60*60),
);

$service_num = 0;
foreach ($services as $service) {
  if (create_service($console, $service_stmt, $service[0], $service[1], $service[2], $service[3])) {
    $service_num++;
  }
}
$console->print("Created $service_num services\n");

// insert bookings
$console->task("Creating bookings");
$booking_query = "INSERT INTO Bookings ( completed, booked_on, starting_time, customer_id, staff_id, service_id )
  VALUES (:completed, :booked_on, :starting_time, :customer_id, :staff_id, :service_id)";

$booking_stmt = $conn->prepare($booking_query);
  
$bookings = array();
$uncompleted_bookings = create_booking_data_after($conn, random_int(105, 155));
$completed_bookings = create_booking_data_before($conn, random_int(105, 155));
$bookings = array_merge($uncompleted_bookings, $completed_bookings);
$booking_num = 0;
if ($completed_bookings == false || $uncompleted_bookings == false) {
  $console->error("Couldn't get random bookings");
} else {
  foreach ($bookings as $booking) {
    if (create_booking($console, $booking_stmt, $conn, $booking["staff_id"], $booking["customer_id"], $booking["service_id"], $booking["booking_time"], $booking["starting_time"], $booking["completed"])) {
      $booking_num++;
    }
  }
}
$console->print("Created $booking_num bookings\n");





$console->end();
  
function create_service($console, $service_stmt, $name, $price, $description, $time_req) {
  // bind the valuess
  $service_stmt->bindParam(":name", $name);
  $service_stmt->bindParam(":price", $price);
  $service_stmt->bindParam(":description", $description);
  $service_stmt->bindParam(":time_req", $time_req);
  
  // execute the query, also check if query was successful
  if($service_stmt->execute()){
    $console->success("Service created ({$name})");
    return true;
  }
  
  $console->error("Couldn't enter service");
  $console->print($service_stmt->errorInfo()[2]);
  return false;
}

function create_customer($console, $customer_stmt, $firstname, $middlename, $lastname, $email, $age, $gender, $address, $phone_no, $password) {
  $started_on = random_int(1541565840, 1573125237);

  // bind the valuess
  $customer_stmt->bindParam(":firstname", $firstname);
  $customer_stmt->bindParam(":middlename", $middlename);
  $customer_stmt->bindParam(":lastname", $lastname);
  $customer_stmt->bindParam(":age", $age);
  $customer_stmt->bindParam(":gender", $gender);
  $customer_stmt->bindParam(":address", $address);
  $customer_stmt->bindParam(":phone_no", $phone_no);
  $customer_stmt->bindParam(":email", $email);
  $customer_stmt->bindParam(":password", $password);
  $customer_stmt->bindParam(":started_on", $started_on);
  
  
  // execute the query, also check if query was successful
  if($customer_stmt->execute()){
    $console->success("Customer created ({$email})");
    return true;
  }
  
  $console->error("Couldn't enter customer");
  $console->print($customer_stmt->errorInfo()[2]);
  return false;
}

function create_staff($console, $staff_stmt, $firstname, $middlename, $lastname, $email, $age, $role, $gender, $phone_no, $password) {
  $started_on = random_int(1541565840, 1573125237);
  $salary = ($role == "staff")? random_int(300, 450) * 100 : random_int(750, 1000) * 100;
  
  // bind the valuess
  $staff_stmt->bindParam(":firstname", $firstname);
  $staff_stmt->bindParam(":middlename", $middlename);
  $staff_stmt->bindParam(":lastname", $lastname);
  $staff_stmt->bindParam(":age", $age);
  $staff_stmt->bindParam(":gender", $gender);
  $staff_stmt->bindParam(":role", $role);
  $staff_stmt->bindParam(":phone_no", $phone_no);
  $staff_stmt->bindParam(":email", $email);
  $staff_stmt->bindParam(":password", $password);
  $staff_stmt->bindParam(":salary", $salary);
  $staff_stmt->bindParam(":started_on", $started_on);
  
  
  // execute the query, also check if query was successful
  if($staff_stmt->execute()){
    $console->success("Staff created ({$email})");
    return true;
  }
  
  $console->error("Couldn't enter staff");
  $console->print($staff_stmt->errorInfo()[2]);
  return false;
}

function create_booking($console, $booking_stmt, $conn, $staff_id, $customer_id, $service_id, $booking_time, $starting_time, $completed) {
  // bind the valuess
  $booking_stmt->bindParam(":staff_id", $staff_id);
  $booking_stmt->bindParam(":customer_id", $customer_id);
  $booking_stmt->bindParam(":service_id", $service_id);
  $booking_stmt->bindParam(":starting_time", $starting_time);
  $booking_stmt->bindParam(":booked_on", $booking_time);
  $zero = 0;
  $booking_stmt->bindParam(":completed", $zero);
  
  // execute the query, also check if query was successful
  if($booking_stmt->execute()){

    if ($completed == 1) {
      $previous_booking_id = $conn->lastInsertId();
      // if completed first insert 0 and then update to 1 (to get experience)
      $booking_update_query = "UPDATE Bookings SET completed = 1 WHERE booking_id = :booking_id";
      $booking_update_stmt = $conn->prepare($booking_update_query);
      $booking_update_stmt->bindParam(":booking_id", $previous_booking_id);
      $booking_update_stmt->execute();
    }
    $console->success("Booking created ({$booking_time})");
    return true;
  }
  
  $console->error("Couldn't enter booking");
  $console->print($booking_stmt->errorInfo()[2]);
  return false;
}

function create_booking_data_before($conn, $required_num = 1) {
  $completed = 1;
  $randoms = array();

  $query = "SELECT customer_id, started_on FROM Customers";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $customers_num = $c->rowCount();
  if (!($customers_num > 0)) {
    return false;
  }
  $customers = array();

  if($customers_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($customers, $row);
    }
  } else {
    return false;
  }
  
  $query = "SELECT service_id, time_req FROM Services";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $services_num = $c->rowCount();
  
  if (!($services_num > 0)) {
    return false;
  }
  $services = array();

  if($services_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($services, $row);
    }
  } else {
    return false;
  }
  
  $query = "SELECT staff_id, started_on FROM Staffs";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $staffs_num = $c->rowCount();
  
  if (!($staffs_num > 0)) {
    return false;
  }
  $staffs = array();

  if($staffs_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($staffs, $row);
    }
  } else {
    return false;
  }

  for ($i = 0; $i < $required_num; $i++) {
    $random_service = $services[random_int(0, $services_num - 1)];  
    $random_customer = $customers[random_int(0, $customers_num - 1)];
    $random_staff = $staffs[random_int(0, $staffs_num - 1)];
    $random_booking_time = random_int($random_customer["started_on"], time() - $random_service["time_req"] - 500);

    // workaround for infinite loops
    $tries = 0;
    
    do {
      $tries++;
      $random_start_time = random_int($random_booking_time, time() - $random_service["time_req"]);
      $random_end_time = $random_start_time + $random_service["time_req"];
      $time_invalid = true;
  
      $query = "SELECT staff_id FROM Staffs WHERE staff_id NOT IN (
        SELECT staff_id FROM Bookings b, Services s WHERE
          b.service_id = s.service_id AND (
            ({$random_start_time} < b.starting_time AND b.starting_time<{$random_end_time}) OR
            ({$random_start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) < {$random_end_time}) OR 
            ({$random_start_time} > b.starting_time AND {$random_end_time} < (b.starting_time + s.time_req))
          )
        )";
      $c = $conn->query($query, PDO::FETCH_ASSOC);
      $c->execute();
      $num = $c->rowCount();
      $available_staffs = array();
      
      if ($num > 0) {
        while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
          // assign values to object properties
          array_push($available_staffs, $row["staff_id"]);
        } 
      }
      
      $query = "SELECT customer_id FROM Customers WHERE customer_id NOT IN (
        SELECT customer_id FROM Bookings b, Services s WHERE
          b.service_id = s.service_id AND (
            ({$random_start_time} < b.starting_time AND b.starting_time<{$random_end_time}) OR
            ({$random_start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) < {$random_end_time}) OR 
            ({$random_start_time} > b.starting_time AND {$random_end_time} < (b.starting_time + s.time_req))
          )
        )";
      $c = $conn->query($query, PDO::FETCH_ASSOC);
      $c->execute();
      $num = $c->rowCount();
      $available_customers = array();
      
      if ($num > 0) {
        while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
          // assign values to object properties
          array_push($available_customers, $row["customer_id"]);
        } 
      }
      
      if (in_array($random_customer["customer_id"], $available_customers) && in_array($random_staff["staff_id"], $available_staffs)) {
        $time_invalid = false;
      }
    } while ($time_invalid && $tries < 500);
  
    $random = array(
      "staff_id" => $random_staff["staff_id"],
      "customer_id" => $random_customer["customer_id"],
      "service_id" => $random_service["service_id"],
      "starting_time" => $random_start_time,
      "booking_time" => $random_booking_time,
      "completed" => $completed
    );
    array_push($randoms, $random);
  }
  return $randoms;
}

function create_booking_data_after($conn, $required_num = 1) {
  $completed = 0;
  $randoms = array();

  $query = "SELECT customer_id, started_on FROM Customers";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $customers_num = $c->rowCount();
  if (!($customers_num > 0)) {
    return false;
  }
  $customers = array();

  if($customers_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($customers, $row);
    }
  } else {
    return false;
  }
  
  $query = "SELECT service_id, time_req FROM Services";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $services_num = $c->rowCount();
  
  if (!($services_num > 0)) {
    return false;
  }
  $services = array();

  if($services_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($services, $row);
    }
  } else {
    return false;
  }
  
  $query = "SELECT staff_id, started_on FROM Staffs";  
  $c = $conn->query($query, PDO::FETCH_ASSOC);
  $c->execute();
  $staffs_num = $c->rowCount();
  
  if (!($staffs_num > 0)) {
    return false;
  }
  $staffs = array();

  if($staffs_num>0){
    while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
      // assign values to object properties
      array_push($staffs, $row);
    }
  } else {
    return false;
  }

  for ($i = 0; $i < $required_num; $i++) {
    $random_service = $services[random_int(0, $services_num - 1)];  
    $random_customer = $customers[random_int(0, $customers_num - 1)];
    $random_staff = $staffs[random_int(0, $staffs_num - 1)];
    $random_booking_time = random_int($random_customer["started_on"], time() - $random_service["time_req"] - 500);

    // workaround for infinite loops
    $tries = 0;
    
    do {
      $tries++;
      $random_start_time = random_int(time() + 5*24*60*60, time() + 50*24*60*60 + $random_service["time_req"]);
      $random_end_time = $random_start_time + $random_service["time_req"];
      $time_invalid = true;
  
      $query = "SELECT staff_id FROM Staffs WHERE staff_id NOT IN (
        SELECT staff_id FROM Bookings b, Services s WHERE
          b.service_id = s.service_id AND (
            ({$random_start_time} < b.starting_time AND b.starting_time<{$random_end_time}) OR
            ({$random_start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) < {$random_end_time}) OR 
            ({$random_start_time} > b.starting_time AND {$random_end_time} < (b.starting_time + s.time_req))
          )
        )";
      $c = $conn->query($query, PDO::FETCH_ASSOC);
      $c->execute();
      $num = $c->rowCount();
      $available_staffs = array();
      
      if ($num > 0) {
        while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
          // assign values to object properties
          array_push($available_staffs, $row["staff_id"]);
        } 
      }
      
      $query = "SELECT customer_id FROM Customers WHERE customer_id NOT IN (
        SELECT customer_id FROM Bookings b, Services s WHERE
          b.service_id = s.service_id AND (
            ({$random_start_time} < b.starting_time AND b.starting_time<{$random_end_time}) OR
            ({$random_start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) < {$random_end_time}) OR 
            ({$random_start_time} > b.starting_time AND {$random_end_time} < (b.starting_time + s.time_req))
          )
        )";
      $c = $conn->query($query, PDO::FETCH_ASSOC);
      $c->execute();
      $num = $c->rowCount();
      $available_customers = array();
      
      if ($num > 0) {
        while ($row = $c->fetch(PDO::FETCH_ASSOC)) {
          // assign values to object properties
          array_push($available_customers, $row["customer_id"]);
        } 
      }
      
      if (in_array($random_customer["customer_id"], $available_customers) && in_array($random_staff["staff_id"], $available_staffs)) {
        $time_invalid = false;
      }
    } while ($time_invalid && $tries < 500);
  
    $random = array(
      "staff_id" => $random_staff["staff_id"],
      "customer_id" => $random_customer["customer_id"],
      "service_id" => $random_service["service_id"],
      "starting_time" => $random_start_time,
      "booking_time" => $random_booking_time,
      "completed" => $completed
    );
    array_push($randoms, $random);
  }
  return $randoms;
}