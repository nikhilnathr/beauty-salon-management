<?php
require_once(__DIR__ ."/api/class/Console.php");
require_once(__DIR__ ."/api/config/core.php");
$KEYS = new Keys();

if (php_sapi_name() === 'cli') {
  // is command interface
  if ($argc != 2) {
    echo "Usage: php ". basename(__FILE__) ." <root_key>\n";
    exit();
  }
  $root_key = $argv[1];
} else if (!empty($_GET['key'])) {
  // is web
  $root_key = $_GET['key'];
} else {
  echo "provide a key";
}
// check if key provided if the root key
if ($root_key !== $KEYS->ROOT_KEY) {
  exit();
}

$console = new Console();
$db = mysqli_connect($KEYS->DB_HOST, $KEYS->DB_USERNAME, $KEYS->DB_PASSWORD, $KEYS->DB_NAME);
if (!$db) {
  $console->fatal("Couldn't connect to database.[Error: ". mysqli_connect_errno() ."]");
}

// delete tables
$console->task("Deleting old tables");
$queries = array(
  "Experiences" => "DROP TABLE IF EXISTS Experiences",
  "Feedbacks" => "DROP TABLE IF EXISTS Feedbacks",
  "Bookings" => "DROP TABLE IF EXISTS Bookings",
  "Services" => "DROP TABLE IF EXISTS Services",
  "Staffs" => "DROP TABLE IF EXISTS Staffs",
  "Customers" => "DROP TABLE IF EXISTS Customers",
);
foreach($queries as $key=>$query) {
  if(!mysqli_query($db, $query)) {
    var_dump(mysqli_error($db));
    $console->fatal("Couldn't delete {$key} table");
    exit();
  }
  $console->success("Deleted {$key} table");
}
$console->print("Deleted all previous tables\n");

// create tables
$console->task("Creating new tables");
$queries = array(
  "Staffs" => "CREATE TABLE IF NOT EXISTS Staffs (
    staff_id INT AUTO_INCREMENT PRIMARY KEY,
    fname VARCHAR(15) NOT NULL,
    mname VARCHAR(15) NULL,
    lname VARCHAR(15) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    age INT NOT NULL,
    role ENUM('admin','staff') DEFAULT 'staff',
    gender ENUM('m', 'f') DEFAULT 'm',
    phone_no BIGINT(12) NOT NULL UNIQUE,
    started_on INT NOT NULL,
    salary DECIMAL(10, 2) NOT NULL,
    password VARCHAR(255) NOT NULL
    )",
  "Customers" => "CREATE TABLE IF NOT EXISTS Customers (
    customer_id INT AUTO_INCREMENT PRIMARY KEY,
    fname VARCHAR(15) NOT NULL,
    mname VARCHAR(15) NULL,
    lname VARCHAR(15) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    age INT NOT NULL,
    gender ENUM('m', 'f') DEFAULT 'm',
    address VARCHAR(255) NULL,
    phone_no BIGINT(12) NOT NULL UNIQUE,
    started_on INT NOT NULL,
    password VARCHAR(255)
  );",
  "Services" => "CREATE TABLE IF NOT EXISTS Services (
    service_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE,
    price DECIMAL(10, 2) NOT NULL,
    description TEXT NULL,
    time_req INT NOT NULL
  );",
  "Bookings" => "CREATE TABLE IF NOT EXISTS Bookings (
    booking_id INT AUTO_INCREMENT PRIMARY KEY,
    completed BOOL default 0,
    booked_on INT NOT NULL,
    starting_time INT NOT NULL,
    customer_id INT NOT NULL,
    staff_id INT NULL,
    service_id INT NULL,
    FOREIGN KEY(customer_id) REFERENCES Customers(customer_id),
    FOREIGN KEY(staff_id) REFERENCES Staffs(staff_id) ON DELETE SET NULL,
    FOREIGN KEY(service_id) REFERENCES Services(service_id) ON DELETE SET NULL
  );",
  "Experiences" => "CREATE TABLE IF NOT EXISTS Experiences (
    staff_id INT NOT NULL,
    service_id INT NOT NULL,
    experience INT DEFAULT 0,
    PRIMARY KEY(staff_id, service_id),
    FOREIGN KEY(staff_id) REFERENCES Staffs(staff_id) ON DELETE CASCADE,
    FOREIGN KEY(service_id) REFERENCES Services(service_id) ON DELETE CASCADE
  );",
  "Feedbacks" => "CREATE TABLE IF NOT EXISTS Feedbacks (
    customer_id INT NULL,
    booking_id INT,
    feedback INT NOT NULL CHECK(feedback >= 1 AND feedback <=5),
    FOREIGN KEY(customer_id) REFERENCES Customers(customer_id) ON DELETE SET NULL,
    FOREIGN KEY(booking_id) REFERENCES Bookings(booking_id) ON DELETE CASCADE
  );",
);
foreach($queries as $key=>$query) {
  if(!mysqli_query($db, $query)) {
    var_dump(mysqli_error($db));
    $console->fatal("Couldn't create {$key} table");
    exit();
  }
  $console->success("Created {$key} table");
}
$console->print("Created all new tables\n");

// create triggers
$console->task("Creating triggers");
$queries = array(
  "add experience after staff insert" => "CREATE TRIGGER add_experiences_staff_insert AFTER INSERT ON Staffs
  FOR EACH ROW
  BEGIN
    IF NEW.role = 'staff' THEN
      INSERT INTO Experiences (staff_id, service_id) SELECT NEW.staff_id, service_id FROM Services;
    END IF;
  END",
  "add experience after service insert" => "CREATE TRIGGER add_experiences_service_insert AFTER INSERT ON Services
  FOR EACH ROW
  BEGIN
    INSERT INTO Experiences (staff_id, service_id) SELECT staff_id, NEW.service_id FROM Staffs WHERE role='staff';
  END;",
  "update experience" => "CREATE TRIGGER update_experiences AFTER UPDATE ON Bookings
  FOR EACH ROW
  BEGIN
    UPDATE Experiences SET experience = (SELECT IFNULL(SUM(s.time_req), 0) FROM Services s, Bookings b WHERE b.service_id = s.service_id AND b.staff_id = NEW.staff_id AND b.completed = 1 AND b.service_id = NEW.service_id) WHERE staff_id = NEW.staff_id AND service_id = new.service_id;
  END;"
);
foreach($queries as $key=>$query) {
  if(!mysqli_query($db, $query)) {
    var_dump(mysqli_error($db));
    $console->error("Couldn't create {$key} trigger");
    $console->msg("Experience for staff maynot be updated");
    exit();
  }
  $console->success("Created {$key} trigger");
}
$console->print("Created all new triggers\n");

// create triggers
$console->task("Creating root user");
// add admin user
$query = "INSERT INTO Staffs ( fname, mname, lname, email, age, role, gender, phone_no, started_on, salary, password ) 
VALUES ( 'Admin', '', '', 'root@localhost', 0, 'admin', 'm', 0000000000, ". time() .", 0, '". password_hash($KEYS->ROOT_PASSWORD, PASSWORD_BCRYPT) ."' )";
if (mysqli_query($db, $query)) {
  $console->success("Created root user");
} else {
  $console->error("Couldn't create root user");
}
$console->print("Created root user\n");


mysqli_close($db);
$console->end();