<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once(__DIR__ . "/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET,PUT");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include classes
require_once(__DIR__ . "/class/Response.php");
require_once(__DIR__ . "/class/Staff.php");
require_once(__DIR__ . "/class/Auth.php");
require_once(__DIR__ . "/config/Database.php");

$res = new Response();
$user = new Auth();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
  // check if already logged in
  if (!$user->checkStatus() || !$user->is_admin) {
    $res->send(401, "", false, "Admin not logged in");
  }
  $db = new Database();
  $conn = $db->connect();

  $new_staff = new Staff($conn);

  $data = json_decode(file_get_contents("php://input"));

  // check for empty values
  if (empty($data->firstname) || empty($data->lastname) || empty($data->email) || empty($data->age)  || empty($data->role)
  || empty($data->gender) || empty($data->phone_no) || empty($data->salary) || empty($data->password)) {
    $res->send(400, "", false, "Required fields are empty");
  }
  
  // sanitize values
  if( filter_var($data->email, FILTER_VALIDATE_EMAIL) != $data->email || !is_numeric($data->age) || strlen($data->phone_no) != 10 || !is_numeric($data->phone_no)
  || !is_numeric($data->salary) || !in_array($data->gender, array("m", "f")) || !in_array($data->role, array("staff", "admin")) ) {
    $res->send(400, "", false, "Invalid value given");
  } else if (strlen($data->password) < 6) {
    $res->send(400, "", false, "Minimum 6 characters for password");
  }
  
  // populate with staff data
  $new_staff->firstname = $data->firstname;
  $new_staff->middlename = empty($data->middlename)? "": $data->middlename;
  $new_staff->lastname = $data->lastname;
  $new_staff->email = $data->email;
  $new_staff->age = intval($data->age);
  $new_staff->gender = $data->gender;
  $new_staff->phone_no = intval($data->phone_no);
  $new_staff->salary = intval($data->salary);
  $new_staff->password = $data->password;
  
  if($new_staff->emailExists()) {
    $res->send(400, "", false, "Email already exists");
  } else if ($new_staff->phoneExists()) {
    $res->send(400, "", false, "Phone number already exists");
  }
  
  $created = ($data->role == "admin")? $new_staff->create(true): $new_staff->create();
  if ($created) {
    $res->send(200, $new_staff->getAllData(), true, "New staff created");
  }
 
  $res->send(400, "", false, $new_staff->get_last_error());
  
  
} else if ($_SERVER["REQUEST_METHOD"] == "GET"){
  if (count($_GET) == 1 && !empty($_GET["id"])) {
    $db = new Database();
    $staff = new Staff($db->connect());
    $staff->staff_id = $_GET["id"];
    
    if ($user->checkStatus() && $user->is_staff) {
      $data = $staff->getAllDataWithExperience();
    } else if ($user->checkStatus() && $user->is_customer) {
      $data = $staff->getPublicDataWithExperience();
    } else {
      $data = $staff->getPublicData();
    }
    // don't show admins data if current user not admin
    if($data !== false && !(!$user->is_admin && $staff->role =="admin")) { 
      $res->send(200, $data);
    }

    $res->send(400, "", false, $staff->get_last_error());

  }else if (count($_GET) == 1 && !empty($_GET["all"])) {
    if($user->checkStatus() && $user->is_admin){
      $db = new Database();
      $staff = new Staff($db->connect());
      $data = $staff->getAllStaffs();
      if($data !== false){
        $res->send(200,$data);
      }
      $res->send(400,"",false,$staff->get_last_error());
    }else{
      $res->send(400,"",false,"Admin not logged in");
    }

  } else {
    // no parameters specified
    if ($user->checkStatus() && $user->is_staff) {
      $db = new Database();
      $staff = new Staff($db->connect());
      $staff->staff_id = $user->staff_id;
      $data = $staff->getAllDataWithExperience();
      if($data !== false) {    
        $res->send(200, $data,true,"Self Details Returned");
      }
      else{
        $res->send(401, "", false, $staff->get_last_error());
      }
    }
    $res->send(401, "", false, "Unauthorised request");

  }
}else if($_SERVER["REQUEST_METHOD"]=="PUT"){
  $db = new Database();
  $staff_ = new Staff($db->connect());

  if(!$user->checkStatus() || !$user->is_admin){
    $res->send(400,"",false,"Admin not logged in");
  }

  $data = json_decode(file_get_contents("php://input"));
  $data->id = $_GET["id"];

  if(!is_numeric($data->id) || filter_var($data->email, FILTER_VALIDATE_EMAIL) != $data->email || !is_numeric($data->age) || strlen($data->phone_no) != 10 || !is_numeric($data->phone_no)
  || !is_numeric($data->salary) || !in_array($data->gender, array("m", "f"))) {
    $res->send(400, "", false, "Invalid value given");
  }

  $staff_->id = intval($data->id);
  $staff_->firstname = $data->firstname;
  $staff_->lastname = $data->lastname;
  $staff_->age = intval($data->age);
  $staff_->email = $data->email;
  $staff_->gender = $data->gender;
  $staff_->phone_no = intval($data->phone_no);
  $staff_->salary = intval($data->salary);

  if($data->role === "admin"){
    if($staff_->updateStaff(true)){
      $res->send(200,"",true,"Successfully Updated");
    }
  }else{
    if($staff_->updateStaff()){
      $res->send(200,"",true,"Successfully Updated");
    }
  }

  $res->send(400,"",false,$staff_->get_last_error());

}else if($_SERVER["REQUEST_METHOD"]=="DELETE"){
  if(count($_GET)==1 && !empty($_GET["id"])){
    $db = new Database();
    $staff = new Staff($db->connect());

    $staff->id = $_GET["id"];

    if(!$user->checkStatus() || !$user->is_admin){
      $res->send(400,"",false,"Admin not logged in");
    }

    if($staff->id == $user->staff_id){
      $res->send(400,"",false,"Cannot delete self");
    }
    if($staff->deleteStaff()){
      $res->send(200,"",true,"Successfully Deleted");
    }
    $res->send(400,"",false,$staff->get_last_error());
  }
}else {
  $res->notFound();
}