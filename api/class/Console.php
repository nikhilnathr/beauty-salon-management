<?php
class Console {
  public $num = array(
    "error" => 0,
    "success" => 0,
    "task" => 0
  );

  function __construct() {
    echo PHP_EOL;
  }

  function print($text) {
    echo "{$text}\n";
  }

  function success($text) {
    $this->num["success"]++;
    echo "\033[1;37m\033[1;42m SUCCESS \033[0m  {$text}\n";
  }
  
  function task($text) {
    $this->num["task"]++;
    echo "\033[0;30m\033[1;43m TASK - {$this->num["task"]} \033[0m  {$text}\n";
  }
  
  function error($text) {
    $this->num["error"]++;
    echo "\033[1;37m\033[1;41m ERROR \033[0m  {$text}\n";
  }

  function end() {
    echo "STATUS: success: {$this->num["success"]}, errors: {$this->num["error"]}\n";
    echo PHP_EOL;
    exit();
  }

  function fatal($text = "") {
    $this->error($text);
    $this->end();
  }
}
