<?php
class Staff{
  // database connection and table name
  private $conn;
  private $table_name = "Staffs";
  private $type = "staff";
  private $last_error = null;
  
  // object properties
  public $id;
  public $firstname;
  public $middlename;
  public $lastname;
  public $age;
  public $gender;
  public $email;
  public $phone_no;
  public $salary;
  public $password;
  public $exps;
  
  // constructor
  public function __construct($db){
    $this->conn = $db;
  }
  
  function create($admin = false){
    
    // insert query
    $query = "INSERT INTO {$this->table_name} 
    (fname, mname, lname, email, age, role, gender, phone_no, started_on, salary, password) 
    VALUES ( :firstname, :middlename, :lastname, :email, :age, :role, :gender, :phone_no, :started_on, :salary, :password )";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);
    
    // sanitize
    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->middlename=htmlspecialchars(strip_tags($this->middlename));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->gender=htmlspecialchars(strip_tags($this->gender));
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $this->salary=htmlspecialchars(strip_tags($this->salary));
    $this->password=htmlspecialchars(strip_tags($this->password));
    
    // bind the values
    $stmt->bindParam(":firstname", $this->firstname);
    $stmt->bindParam(":middlename", $this->middlename);
    $stmt->bindParam(":lastname", $this->lastname);
    $stmt->bindParam(":age", $this->age);
    $stmt->bindParam(":gender", $this->gender);
    $stmt->bindParam(":phone_no", $this->phone_no);
    $stmt->bindParam(":salary", $this->salary);
    $stmt->bindParam(":email", $this->email);
    
    // assign the role to be decided
    $this->role = ($admin)? "admin": "staff";
    $stmt->bindParam(":role", $this->role);
    
    // save the creation time of staff
    $this->started_on = time();
    $stmt->bindParam(":started_on", $this->started_on);
    
    // hash the password before saving to database
    $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
    $stmt->bindParam(":password", $password_hash);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      $this->staff_id = $this->conn->lastInsertId();
      return true;
    }
    
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }
  
  function get_last_error() {
    return $this->last_error;
  }
  
  // check if a phone number already exists
  function phoneExists() {
    $query = "SELECT staff_id FROM {$this->table_name} WHERE phone_no = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $stmt->bindParam(1, $this->phone_no);
    
    // execute the query
    if(!$stmt->execute()){
      $this->last_error=$stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();
    
    return ($num>0);
  }
  
  function fetchAllData() {
    $query = "SELECT staff_id, fname, mname, lname, email, gender, salary, age, phone_no, started_on, role FROM {$this->table_name} WHERE staff_id = ? LIMIT 1";
    $query_exp = "SELECT a.service_id, a.experience, b.name FROM Experiences a LEFT JOIN Services b ON a.service_id = b.service_id WHERE a.staff_id = ?;";
    
    $stmt = $this->conn->prepare( $query );
    $stmt_exp = $this->conn->prepare( $query_exp );

    $this->staff_id=htmlspecialchars(strip_tags($this->staff_id));
    $stmt->bindParam(1, $this->staff_id);
    $stmt->bindParam(1, $this->staff_id);

    // execute the query
    if(!$stmt->execute()){
      $this->last_error=$stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();
    $num_exp = $stmt_exp->rowCount();
    
    while($num_exp>0){
      //get row values
      $row_exp = $stmt_exp->fetch(PDO::FETCH_ASSOC);
      //add experience of each guy;
      array_push($exps,array(
        "service_id" => $row_exp["service_id"],
        "name"=>$row_exp["name"],
        "experience"=>$row_exp["experience"]
      ));
      
      $num_exp--;
    }
    
    if($num>0){
      // get record details / values
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      
      // assign values to object properties
      $this->staff_id = $row["staff_id"];
      $this->firstname = $row["fname"];
      $this->middlename = empty($row["mname"])? "": $row["mname"];
      $this->lastname = $row["lname"];
      $this->email = $row["email"];
      $this->gender = $row["gender"];
      $this->salary = $row["salary"];
      $this->age = $row["age"];
      $this->phone_no = $row["phone_no"];
      $this->started_on = $row["started_on"];
      $this->role = $row["role"];      
    }
    
    $this->last_error = null;
    return true;
  }
  
  function fetchExperiences() {
    $query_services = "SELECT service_id, name FROM Services";
      $stmt = $this->conn->prepare( $query_services );

      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
      $num = $stmt->rowCount();

      $services = array();
      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
          $services[$service_id] = $name;
        }
      }

      $query_experience = "SELECT service_id, experience FROM Experiences WHERE staff_id = ?";
      
      $stmt = $this->conn->prepare( $query_experience );

      $this->staff_id=htmlspecialchars(strip_tags($this->staff_id));
      $stmt->bindParam(1, $this->staff_id);
      
      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
      $num = $stmt->rowCount();

      $experiences = array();
      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $row["name"] = $services[$row["service_id"]];
          array_push($experiences, $row);
        }
      }
      
      $this->last_error=null;
      return $experiences;
  }
  
  function getAllData() {
    if ((!empty($this->staff_id) && !empty($this->firstname) && !empty($this->lastname) && !empty($this->email) && !empty($this->age)  && 
    !empty($this->gender) && !empty($this->phone_no) && !empty($this->salary) && !empty($this->password))  || $this->fetchAllData()) {
      
      $this->last_error=null;
      return json_decode(json_encode(array(
        "staff_id" => $this->staff_id,
        "firstname" => $this->firstname,
        "middlename" => $this->middlename,
        "lastname" => $this->lastname,
        "gender" => $this->gender,
        "salary" => $this->salary,
        "age"  => $this->age,
        "phone_no" => $this->phone_no,
        "started_on" => $this->started_on,
        "role" => $this->role,
        "type" => $this->type,
        "experience" =>$this->exps
      )));
    }
    return false;
  }

  function getAllDataWithExperience() {
    if ($data = $this->getAllData()) {
      $experiences = $this->fetchExperiences();
      $data->experiences = $experiences;
      return $data;
      $this->last_error=null;
    }
    return false;
  }


  function getPublicDataWithExperience() {
    if ($data = $this->getPublicData()) {
      $experiences = $this->fetchExperiences();
      $data->experiences = $experiences;
      $this->last_error=null;
      return $data;
    }
    return false;
  }

  function getPublicData() {
    if($this->getAllData()) {
      return json_decode(json_encode(array(
        "staff_id" => $this->staff_id,
        "firstname" => $this->firstname,
        "lastname" => $this->lastname,
        "gender" => $this->gender,
        "phone_no" => $this->phone_no,
        "role" => $this->role,
        "type" => $this->type,
        "experience" =>$this->exps
      )));
    }
    return false;
  }

  function getAllStaffs(){
    $query = "SELECT staff_id, fname, mname, lname, email, gender, salary, age, phone_no, started_on, role FROM {$this->table_name} ORDER BY staff_id;";
    $stmt = $this->conn->prepare($query);

    // execute the query
    if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
    $num = $stmt->rowCount();

    $staffa = array();

    while($num > 0){
      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      array_push($staffa,array(
        "staff_id"=>$row["staff_id"],
        "name"=>$row["fname"]." ".$row["lname"],
        "email"=>$row["email"],
        "gender"=>$row["gender"],
        "salary"=>$row["salary"],
        "age"=>$row["age"],
        "phone_no"=>$row["phone_no"],
        "started_on"=>$row["started_on"],
        "role"=>$row["role"]
      ));

      $num--;
    }

    $this->last_error=null;
    return json_decode(json_encode($staffa));
  }

  // check if given email exist in the database
  function emailExists(){
    $query = "SELECT staff_id FROM {$this->table_name} WHERE email = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->email=htmlspecialchars(strip_tags($this->email));
    $stmt->bindParam(1, $this->email);
    
    // execute the query
    if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
    }
    $num = $stmt->rowCount();

    $this->last_error=null;    
    return ($num>0);
  }

  function deleteStaff(){
    if(empty($this->id)){
      $this->last_error="Staff_id not found";
      return false;
    }

    if($this->id==1){
      $this->last_error = "Root admin cannot be deleted";
      return false;
    }

    $query_check = "select booking_id from Bookings where staff_id = {$this->id} and completed=0";
    $stmt_check = $this->conn->prepare($query_check);

    if(!$stmt_check->execute()){
      $this->last_error = $stmt_check->errorInfo()[2];
      return false;
  }
    $num = $stmt_check->rowCount();

    if($num>0){
      $this->last_error="Booking for this staff is available, unable to delete";
      return false;
    }

    $query = "DELETE from {$this->table_name} where staff_id={$this->id}";

    $stmt = $this->conn->prepare($query);
    if($stmt->execute()){
      $this->last_error = null;
      return true;
    }
    $this->last_error=$stmt->errorInfo()[2];
    return false;
  }

  function updateStaff($admin = false){
    if(empty($this->id) || empty($this->firstname) || empty($this->lastname) || empty($this->email) || 
      empty($this->age) || empty($this->gender) || empty($this->phone_no) || empty($this->salary)){
        $this->last_error ="Required fields are empty";
        return false;
    }
    
    // insert query
    $query = "UPDATE {$this->table_name} 
      set fname = :firstname,lname = :lastname, age = :age,email = :email,
      gender = :gender, phone_no = :phone_no, salary = :salary, role = :role 
      where staff_id={$this->id}";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->gender=htmlspecialchars(strip_tags($this->gender));
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $this->salary=htmlspecialchars(strip_tags($this->salary));
    
    // bind the values
    $stmt->bindParam(":firstname", $this->firstname);
    $stmt->bindParam(":lastname", $this->lastname);
    $stmt->bindParam(":age", $this->age);
    $stmt->bindParam(":gender", $this->gender);
    $stmt->bindParam(":phone_no", $this->phone_no);
    $stmt->bindParam(":salary", $this->salary);
    $stmt->bindParam(":email", $this->email);

    // assign the role to be decided
    $this->role = ($admin)? "admin": "staff";
    $stmt->bindParam(":role", $this->role);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      return true;
    }
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }

}