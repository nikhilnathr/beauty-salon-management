<?php
require_once(__DIR__ . "/Customer.php");
require_once(__DIR__ . "/Staff.php");

class Auth {
  
  public $staff_id;
  public $customer_id;
  public $email;
  public $type;
  private $conn;

  public $is_customer;
  public $is_staff;
  public $is_admin = false;
  public $role = null;
  
  function __construct($db = null) {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
    $this->conn = $db;
  }
  
  function validateCredential($password, $type) {
    if ($this->checkStatus()) {
      return true;
    }

    if ($type == "staff") {
      $query = "SELECT staff_id, email, role, password FROM Staffs WHERE email = ? LIMIT 1";
      
      $stmt = $this->conn->prepare( $query );
      
      $this->email=htmlspecialchars(strip_tags($this->email));
      $stmt->bindParam(1, $this->email);
      
      // execute the query
      $stmt->execute();
      $num = $stmt->rowCount();
      
      if($num>0){
        
        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if(password_verify($password, $row["password"]) === true) {
          $this->is_customer = false;
          $this->is_staff = true;
          
          $_SESSION["id"] = $row["staff_id"];
          $_SESSION["email"] = $row["email"];
          $_SESSION["type"] = $type;
          $_SESSION["role"] = $row["role"];
          
          // assign values to object properties
          $this->staff_id = $row["staff_id"];
          $this->email = $row["email"];
          $this->type = $type;
          $this->role = $row["role"];
          
          $this->is_admin = ($this->role == "admin")? true: false;
          
          return true;
        }    
      }
    } else if ($type == "customer") {
      $query = "SELECT customer_id, email, password FROM Customers WHERE email = ? LIMIT 1";
      
      $stmt = $this->conn->prepare( $query );
      
      $this->email=htmlspecialchars(strip_tags($this->email));
      $stmt->bindParam(1, $this->email);
      
      // execute the query
      $stmt->execute();
      $num = $stmt->rowCount();
      
      if($num>0){
        
        // get record details / values
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if(password_verify($password, $row["password"]) === true) {
          $this->is_customer = true;
          $this->is_staff = false;
          
          $_SESSION["id"] = $row["customer_id"];
          $_SESSION["email"] = $row["email"];
          $_SESSION["type"] = $type;
          
          // assign values to object properties
          $this->customer_id = $row["customer_id"];
          $this->email = $row["email"];
          $this->type = $type;
          
          return true;
        }    
      }
    }
    
    return false;
  }
  
  function checkStatus() {
    if (!(empty($_SESSION["id"]) || empty($_SESSION["type"]) || empty($_SESSION["email"]))) {
      $this->role = (!empty($_SESSION["role"]))? $_SESSION["role"]: null;
      $this->is_admin = ($this->role == "admin")? true: false;
      $this->email = $_SESSION["email"];
      $this->type = $_SESSION["type"];
      if ($this->type == "staff") {
        $this->is_customer = false;
        $this->is_staff = true;
        $this->staff_id = $_SESSION["id"];
      } else {
        $this->is_customer = true;
        $this->is_staff = false;
        $this->customer_id = $_SESSION["id"];
      }
      return true;
    }
    return false;
  }
  
  function logout() {
    $this->checkStatus();
    $_SESSION = array();
  }
  
  function loginUser(){
    return false;
  }
  
}