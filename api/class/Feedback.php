<?php
class Feedback{
	private $conn;
	private $table_name = "Feedbacks";
	private $last_error = null;

	public $customer_id;
	public $service_id;
	public $feedback;

	public function _construct($db){
		$this->conn = $db;
	}

	public function create(){
		if(empty($this->customer_id) || empty($this->service_id) || empty($this->feedback)){
			$this->last_error = "Required fields are empty";
			return false;
		}
		$query = "INSERT INTO {$this->table_name} (
			customer_id,
			service_id,
			feedback) VALUES(
			:cust_id,
			:ser_id,	
			:feed
		);";

		//prepare query
		$stmt = $this->conn->prepare($query);

		//sanitize
		$this->customer_id = htmlspecialchars(strip_tags($this->customer_id));
		$this->service_id = htmlspecialchars(strip_tags($this->service_id));
		$this->feedback = htmlspecialchars(strip_tags($this->feedback));

		//bind params
		$stmt->bindParam(":cust_id",$this->customer_id);
		$stmt->bindParam(":ser_id",$this->service_id);
		$stmt->bindParam("feed",$this->feedback);

		if($stmt->execute()){
			$this->last_error = null;
			return true;
		}
		$this->last_error = $stmt->errorInfo()[2];
		return false;
	}

	function getLastError(){
		return $this->last_error;
	}

	function getServiceFeedback(){
		if(empty($this->service_id)){
			$this->last_error = "service_id not present";
			return false;
		}

		$query = "SELECT a.customer_id,a.feedback,b.fname,b.lname FROM {$this->table_name} a, Customers b WHERE a.service_id=? AND a.customer_id = b.customer_id;";
		$stmt = $this->conn->prepare($query);

		$this->service_id = htmlspecialchars(strip_tags($this->service_id));
		$stmt->bindParam(1,$this->service_id);

		$serar = array();

		$stmt->execute();
		$num = $stmt->rowCount();
		while($num>0){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			array_push($serar,array(
				"customer_id"=>$row["customer_id"],
				"customer_name"=>$row["fname"]." ".$row["lname"],
				"feedback"=>$row["feedback"]
			));

			$num --;
		}
		$this->last_error = null;
		return json_decode(json_encode($serar));
	}
	function deleteServiceFeedback(){

		if(empty($this->service_id) || empty($this->customer_id)){
			$this->last_error="Required fields are not present";
			return false;
		}

		$query = "DELETE from {$this->table_name} where customer_id={$this->customer_id} and service_id={$this->service_id};";
		$stmt = $this->conn->prepare($query);

		if($stmt->execute()){
			$this->last_error = null;
			return true;
		}
		$this->last_error=$stmt->errorInfo()[2];
		return false;
	}
}