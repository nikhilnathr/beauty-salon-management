<?php
class Customer {
  
  // database connection and table name
  private $conn;
  private $table_name = "Customers";
  private $type = "customer";
  private $last_error = null;
  
  // object properties
  public $id;
  public $firstname;
  public $middlename;
  public $lastname;
  public $password;
  
  // constructor
  public function __construct($db){
    $this->conn = $db;
  }
  
  function create($admin = false){
    
    // insert query
    $query = "INSERT INTO {$this->table_name}(
      fname,
      mname,
      lname,
      email,
      age,
      gender,
      address,
      phone_no,
      started_on,
      password
      ) VALUES (
      :firstname,
      :middlename,
      :lastname,
      :email,
      :age,
      :gender,
      :address,
      :phone_no,
      :started_on,
      :password 
    )";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->middlename=htmlspecialchars(strip_tags($this->middlename));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->gender=htmlspecialchars(strip_tags($this->gender));
    $this->address=htmlspecialchars(strip_tags($this->address));
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $this->password=htmlspecialchars(strip_tags($this->password));
    
    // bind the values
    $stmt->bindParam(":firstname", $this->firstname);
    $stmt->bindParam(":middlename", $this->middlename);
    $stmt->bindParam(":lastname", $this->lastname);
    $stmt->bindParam(":age", $this->age);
    $stmt->bindParam(":gender", $this->gender);
    $stmt->bindParam(":address", $this->address);
    $stmt->bindParam(":phone_no", $this->phone_no);
    $stmt->bindParam(":email", $this->email);
    
    // save the creation time of customer
    $this->started_on = time();
    $stmt->bindParam(":started_on", $this->started_on);

    // hash the password before saving to database
    $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
    $stmt->bindParam(":password", $password_hash);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      $this->customer_id = $this->conn->lastInsertId();
      return true;
    }
    
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }

  function get_last_error() {
    return $this->last_error;
  }

  // check if a phone number already exists
  function phoneExists() {
    $query = "SELECT customer_id FROM {$this->table_name} WHERE phone_no = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $stmt->bindParam(1, $this->phone_no);

    // execute the query
    $stmt->execute();
    $num = $stmt->rowCount();
    
    return ($num>0);
  }

  function fetchAllData() {
    $query = "SELECT customer_id, fname, mname, lname, email, gender, address, age, phone_no, started_on FROM {$this->table_name} WHERE customer_id = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );

    $this->customer_id=htmlspecialchars(strip_tags($this->customer_id));
    $stmt->bindParam(1, $this->customer_id);

    // execute the query
    if(!$stmt->execute()){
      $this->last_error = $stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();
    
    if($num>0){
      
      // get record details / values
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      
      // assign values to object properties
      $this->customer_id = $row["customer_id"];
      $this->firstname = $row["fname"];
      $this->middlename = empty($row["mname"])? "": $row["mname"];
      $this->lastname = $row["lname"];
      $this->email = $row["email"];
      $this->gender = $row["gender"];
      $this->age = $row["age"];
      $this->address = empty($row["address"])? "": $row["address"];
      $this->phone_no = $row["phone_no"];
      $this->started_on = $row["started_on"];
      
    }
    $this->last_error = null;
    return true;
  }

  function getAllData() {
    if ((!empty($this->firstname) && !empty($this->lastname) && !empty($this->email) && !empty($this->age)  && 
    !empty($this->gender) && !empty($this->phone_no) && !empty($this->password)) || $this->fetchAllData()) {
      return json_decode(json_encode(array(
        "customer_id" => $this->customer_id,
        "firstname" => $this->firstname,
        "middlename" => $this->middlename,
        "lastname" => $this->lastname,
        "email" => $this->email,
        "gender" => $this->gender,
        "age"  => $this->age,
        "address"  => $this->address,
        "phone_no" => $this->phone_no,
        "started_on" => $this->started_on,
        "type" => $this->type
      )));
    }
    return false;
  }

  function getPublicData() {
    if ($this->getAllData()) {
      return json_decode(json_encode(array(
        "customer_id" => $this->customer_id,
        "firstname" => $this->firstname,
        "lastname" => $this->lastname,
        "gender" => $this->gender,
        "phone_no" => $this->phone_no,
        "type" => $this->type
      )));
    }
    return false;
  }

  function getAllCustomerData(){
    $query = "SELECT customer_id, fname, mname, lname, email, gender, address, age, phone_no, started_on FROM {$this->table_name} ORDER BY customer_id;";
    $stmt = $this->conn->prepare($query);

    if(!$stmt->execute()){
      $this->last_error = $stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();

    $cus = array();
    
    while($num > 0){
      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      array_push($cus,array(
        "customer_id"=>$row["customer_id"],
        "name"=>$row["fname"]." ".$row["lname"],
        "email"=>$row["email"],
        "gender"=>$row["gender"],
        "address"=>$row["address"],
        "age"=>$row["age"],
        "phone_no"=>$row["phone_no"],
        "started_on"=>$row["started_on"]
      ));
      $num--;
    }
    $this->last_error = null;
    return json_decode(json_encode($cus));
  }

  // check if given email exist in the database
  function emailExists(){
    $query = "SELECT customer_id FROM {$this->table_name} WHERE email = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->email=htmlspecialchars(strip_tags($this->email));
    $stmt->bindParam(1, $this->email);

    // execute the query
    $stmt->execute();
    $num = $stmt->rowCount();

    $this->last_error = null;
    return ($num>0);
  }

  function deleteCustomer(){
    if(empty($this->id)){
      $this->last_error="Customer_id not found";
      return false;
    }
  $query = "DELETE from {$this->table_name} where customer_id={$this->id}";

  $stmt = $this->conn->prepare($query);
	if($stmt->execute()){
		$this->last_error = null;
		return true;
	}
	$this->last_error=$stmt->errorInfo()[2];
	return false;
  }

  function updateCustomer(){
    if(empty($this->customer_id)){
      $this->last_error = "Customer Id not found";
      return false;
    }
     // insert query
     $query = "INSERT INTO {$this->table_name}(
      fname,
      mname,
      lname,
      email,
      age,
      gender,
      address,
      phone_no,
      started_on,
    ) VALUES (
      :firstname,
      :middlename,
      :lastname,
      :email,
      :age,
      :gender,
      :address,
      :phone_no,
      :started_on,
    )";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->firstname=htmlspecialchars(strip_tags($this->firstname));
    $this->middlename=htmlspecialchars(strip_tags($this->middlename));
    $this->lastname=htmlspecialchars(strip_tags($this->lastname));
    $this->email=htmlspecialchars(strip_tags($this->email));
    $this->age=htmlspecialchars(strip_tags($this->age));
    $this->gender=htmlspecialchars(strip_tags($this->gender));
    $this->address=htmlspecialchars(strip_tags($this->address));
    $this->phone_no=htmlspecialchars(strip_tags($this->phone_no));
    $this->password=htmlspecialchars(strip_tags($this->password));
    
    // bind the values
    $stmt->bindParam(":firstname", $this->firstname);
    $stmt->bindParam(":middlename", $this->middlename);
    $stmt->bindParam(":lastname", $this->lastname);
    $stmt->bindParam(":age", $this->age);
    $stmt->bindParam(":gender", $this->gender);
    $stmt->bindParam(":address", $this->address);
    $stmt->bindParam(":phone_no", $this->phone_no);
    $stmt->bindParam(":email", $this->email);
    
    // save the creation time of customer
    $this->started_on = time();
    $stmt->bindParam(":started_on", $this->started_on);

    // hash the password before saving to database
    $password_hash = password_hash($this->password, PASSWORD_BCRYPT);
    $stmt->bindParam(":password", $password_hash);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      return true;
    }
    
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }
}