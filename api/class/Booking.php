<?php

class Booking{
	//database connection and table name
	private $conn;
	private $table_name = "Bookings";
	private $last_error = null;

	//object properties
	public $id;
	public $feedback;
	public $isAdmin = false;
	public $booked_on;
	public $start_time;
	public $customer_id;
	public $staff_id;
	public $service_id;
	public $timeReq;
	public $customer_name;
	public $staff_name;
	public $service_name;
	public $end_time;
	

	//constructor
	public function __construct($db){
		$this->conn = $db;
	}

	function createBooking(){
	
		//insert query
		$query = "INSERT INTO {$this->table_name}(
			booked_on,
			starting_time,
			customer_id,
			staff_id,
			service_id)
			 VALUES (
			:booked_on,
			:start_time,
			:customer_id,
			:staff_id,
			:service_id
		)";

		$this->booked_on = time();

		//prepare query

		$stmt = $this->conn->prepare($query);

		//sanitize
		$this->start_time = htmlspecialchars(strip_tags($this->start_time));
		$this->customer_id = htmlspecialchars(strip_tags($this->customer_id));
		$this->staff_id = htmlspecialchars(strip_tags($this->staff_id));
		$this->service_id = htmlspecialchars(strip_tags($this->service_id));


		$query_service = "SELECT time_req FROM Services WHERE service_id = ? LIMIT 1";
    
		$stmt_service = $this->conn->prepare( $query_service );
		
		$this->service_id=htmlspecialchars(strip_tags($this->service_id));
		$stmt_service->bindParam(1, $this->service_id);
		
		// execute the query
		if(!$stmt_service->execute()){
		  $this->last_error = $stmt_service->errorInfo()[2];
		  return false;
		}

		$row_service = $stmt_service->fetch(PDO::FETCH_ASSOC);

		$time_req = $row_service["time_req"];

		$this->end_time = $this->start_time + $time_req;

		$query_time_check = "SELECT b.service_id FROM Bookings b, Services s WHERE b.staff_id = {$this->staff_id} AND b.service_id = s.service_id AND (({$this->start_time} <= b.starting_time AND b.starting_time < {$this->end_time})
			OR ({$this->start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) <= {$this->end_time}) OR ({$this->start_time} >= (b.starting_time) AND {$this->end_time} <= (b.starting_time + s.time_req)) OR 
			({$this->start_time} > b.starting_time AND {$this->end_time} < (b.starting_time + s.time_req)))";

		$stmt_time_check = $this->conn->prepare($query_time_check);

		if(!$stmt_time_check->execute()){
			$this->last_error = $stmt_time_check->errorInfo()[2];
			return false;
		}

		if($stmt_time_check->rowCount() > 0){
			$this->last_error = "This staff is busy at the time";
			return false;
		}

		//bindparams
		$stmt->bindParam(":booked_on",$this->booked_on);
		$stmt->bindParam(":start_time",$this->start_time);
		$stmt->bindParam(":customer_id",$this->customer_id);
		$stmt->bindParam(":staff_id",$this->staff_id);
		$stmt->bindParam(":service_id",$this->service_id);

		//execute
		if($stmt->execute()){
			$this->last_error=null;
			$this->id = $this->conn->lastInsertId();
			return true;
		}
		
		$this->last_error = $stmt->errorInfo()[2];
		return false;
	}

	function getLastError(){
		return $this->last_error;
	}

	function fetchAllData(){
		//query
		$query = "SELECT a.booking_id, a.completed, a.starting_time, a.customer_id, a.booked_on, f.feedback, a.staff_id, a.service_id, cus.fname AS c_fname, cus.lname AS c_lname, st.fname AS s_fname,st.lname AS s_lname,s.name AS ser_name,s.price,s.time_req
		 FROM Bookings AS a LEFT JOIN Customers AS cus ON a.customer_id=cus.customer_id LEFT JOIN Staffs AS st ON a.staff_id=st.staff_id LEFT JOIN Services AS s AS a.service_id=s.service_id LEFT JOIN Feedbacks AS f ON f.booking_id = a.booking_id WHERE a.booking_id=?;";

		$stmt = $this->conn->prepare($query);
		$this->id = htmlspecialchars(strip_tags($this->id));
		$stmt->bindParam(1,$this->id);

		if(!$stmt->execute()){
			$this->last_error = $stmt->errorInfo()[2];
			return false;
		}
		$num = $stmt->rowCount();

		if($num>0){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if(empty($row["staff_id"])){
				$row["staff_id"]="";
				$row["s_fname"] = "Deleted";
				$row["s_lname"] = "Staff";
			}
			if(empty($row["customer_id"])){
				$row["customer_id"]="";
				$row["c_fname"] = "Deleted";
				$row["c_lname"] = "Customer";
			}
			if(empty($row["service_id"])){
				$row["service_id"] = "";
				$row["ser_name"] = "Deleted";
				$row["price"] = "";
				$row["time_req"] = "";
			}

			$this->id = $row["booking_id"];
			$this->completed = boolval($row["completed"]);
			$this->booked_on = $row["booked_on"];
			$this->start_time = $row["starting_time"];
			$this->customer_id = $row["customer_id"];
			$this->customer_name = $row["c_fname"]." ".$row["c_lname"];
			$this->staff_id = $row["staff_id"];
			$this->staff_name = $row["s_fname"]." ".$row["s_lname"];
			$this->service_id = $row["service_id"];
			$this->price = $row["price"];
			$this->timeReq = $row["time_req"];
			$this->feedback = empty($row["feedback"]) ? 0 : intval($row["feedback"]);
			$this->service_name = $row["ser_name"];

		}
		$this->last_error=null;
		return true;
	}

	function getAllData(){
		if((!empty($this->id) && !empty($this->completed) && !empty($this->booked_on) && !empty($this->start_time) && !empty($this->customer_id)
		&& !empty($this->staff_id) && !empty($this->service_id) && !empty($this->customer_name) && !empty($this->service_name)
		&& !empty($this->staf_name)) || $this->fetchAllData()){
			return json_decode(json_encode(array(
				"booking_id"=>$this->id,
				"completed"=>$this->completed,
				"starting_time"=>$this->start_time,
				"customer_id"=>$this->customer_id,
				"customer_name"=>$this->customer_name,
				"staff_id"=>$this->staff_id,
				"staff_name"=>$this->staff_name,
				"service_id"=>$this->service_id,
				"price"=>$this->price,
				"feedback"=> $this->feedback,
				"time_req"=>$this->timeReq,
				"service_name"=>$this->service_name
			)));
		}

		return false;
	}

	function getPublicData(){
		if($this->getAllData()){
			return json_decode(json_encode(array(
				"booking_id"=>$this->id,
				"completed"=>$this->completed,
				"starting_time"=>$this->start_time,
				"customer_id"=>$this->customer_id,
				"customer_name"=>$this->customer_name,
				"staff_id"=>$this->staff_id,
				"staff_name"=>$this->staff_name,
				"service_id"=>$this->service_id,
				"price"=>$this->price,
				"feedback"=>$this->feedback,
				"time_req"=>$this->timeReq,
				"service_name"=>$this->service_name
			)));
		}
		return false;
	}

	function getCustomerBooking(){
		if(!empty($this->customer_id)){
			//query
			$query = "SELECT a.booking_id, a.completed, f.feedback, a.starting_time, a.customer_id, a.booked_on, a.staff_id, a.service_id, st.fname AS s_fname,st.lname AS s_lname,s.name AS ser_name,s.price,s.time_req
			 FROM Bookings AS a LEFT JOIN Staffs AS st ON a.staff_id=st.staff_id LEFT JOIN Services AS s ON a.service_id = s.service_id LEFT JOIN Feedbacks AS f ON f.booking_id = a.booking_id WHERE a.customer_id=?;";

			//prepare
			$stmt = $this->conn->prepare($query);
			$this->customer_id = htmlspecialchars(strip_tags($this->customer_id));
			$stmt->bindParam(1,$this->customer_id);

			if(!$stmt->execute()){
				$this->last_error = $stmt->errorInfo()[2];
				return false;
			}
			$num = $stmt->rowCount();

			$cusBook = array();

			while($num>0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				if(empty($row["staff_id"])){
					$row["staff_id"]="";
					$row["s_fname"] = "Deleted";
					$row["s_lname"] = "Staff";
				}
				if(empty($row["service_id"])){
					$row["service_id"] = "";
					$row["ser_name"] = "Deleted";
					$row["price"] = "";
					$row["time_req"] = "";
				}
	

				array_push($cusBook,array(
					"booking_id"=>$row["booking_id"],
					"completed"=>boolval($row["completed"]),
					"booked_on"=>$row["booked_on"],
					"starting_time"=>$row["starting_time"],
					"customer_id"=>$row["customer_id"],
					"staff_id"=>$row["staff_id"],
					"staff_name"=>$row["s_fname"]." ".$row["s_lname"],
					"service_id"=>$row["service_id"],
					"price"=>$row["price"],
					"feedback"=> empty($row["feedback"]) ? 0 : intval($row["feedback"]),
					"time_req"=>$row["time_req"],
					"service_name"=>$row["ser_name"]
				));

				$num--;
			}

			$this->last_error=null;
			return $cusBook;
		
		}
		$this->last_error = "CustomerId required";
		return false;
	}
	function getCustomerBookData(){	
		$data = $this->getCustomerBooking();
		if($data!==false){
			return json_decode(json_encode($data));
		}
		return false;
	}

	function getCustomerPublicBookData(){
		$data = $this->getCustomerBooking();
		if($data!==false){
			return json_decode(json_encode($data));
		}
		return false;
	}

	function getStaffBooking(){
		if(!empty($this->staff_id)){
			//query
			$query = "SELECT a.booking_id, a.completed, f.feedback, a.starting_time, a.customer_id, a.booked_on, a.staff_id, a.service_id, cus.fname AS c_fname, cus.lname AS c_lname, s.name AS ser_name,s.price,s.time_req
			 FROM Bookings AS a LEFT JOIN Customers AS cus ON a.customer_id=cus.customer_id LEFT JOIN Services AS s ON a.service_id=s.service_id LEFT JOIN Feedbacks AS f ON f.booking_id = a.booking_id WHERE a.staff_id=?;";

			//prepare
			$stmt = $this->conn->prepare($query);
			$this->staff_id = htmlspecialchars(strip_tags($this->staff_id));
			$stmt->bindParam(1,$this->staff_id);

			if(!$stmt->execute()){
				$this->last_error = $stmt->errorInfo()[2];
				return false;
			}
			$num = $stmt->rowCount();

			$cusBook = array();

			while($num>0){
				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				if(empty($row["customer_id"])){
					$row["customer_id"]="";
					$row["c_fname"] = "Deleted";
					$row["c_lname"] = "Customer";
				}
				if(empty($row["service_id"])){
					$row["service_id"] = "";
					$row["ser_name"] = "Deleted";
					$row["price"] = "";
					$row["time_req"] = "";
				}
		
				array_push($cusBook,array(
					"booking_id"=>$row["booking_id"],
					"completed"=>boolval($row["completed"]),
					"booked_on"=>$row["booked_on"],
					"starting_time"=>$row["starting_time"],
					"customer_id"=>$row["customer_id"],
					"customer_name"=>$row["c_fname"]." ".$row["c_lname"],
					"staff_id"=>$row["staff_id"],
					"service_id"=>$row["service_id"],
					"feedback"=> empty($row["feedback"]) ? 0 : intval($row["feedback"]),
					"price"=>$row["price"],
					"time_req"=>$row["time_req"],
					"service_name"=>$row["ser_name"]
				));

				$num--;
			}
			$this->last_error=null;
			return $cusBook;
		}
		$this->last_error = "Staff_id required";
		return false;
	}


	function getStaffBookData(){
		$data = $this->getStaffBooking();

		if($data!==false){
			return json_decode(json_encode($data));
		}
		return false;
	}

	function getStaffPublicBookData(){
		$data = $this->getStaffBooking();
		if($data!==false){
			return json_decode(json_encode($data));
		}
		return false;
	}

	function getAllBookings(){
		//query
		$query = "SELECT a.booking_id, a.completed, f.feedback, a.starting_time, a.customer_id, a.booked_on, a.staff_id, a.service_id, cus.fname AS c_fname, cus.lname AS c_lname, st.fname AS s_fname,st.lname AS s_lname,s.name AS ser_name,s.price,s.time_req
		 FROM Bookings AS a LEFT JOIN Customers AS cus ON a.customer_id=cus.customer_id LEFT JOIN Staffs AS st ON a.staff_id=st.staff_id LEFT JOIN Services AS s ON a.service_id = s.service_id LEFT JOIN Feedbacks AS f ON f.booking_id = a.booking_id ;";

		//prepare
		$stmt = $this->conn->prepare($query);
		if(!$stmt->execute()){
			$this->last_error = $stmt->errorInfo()[2];
			return true;
		}
		$num = $stmt->rowCount();

		$cusBook = array();

		while($num>0){
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if(empty($row["staff_id"])){
				$row["staff_id"]="";
				$row["s_fname"] = "Deleted";
				$row["s_lname"] = "Staff";
			}
			if(empty($row["customer_id"])){
				$row["customer_id"]="";
				$row["c_fname"] = "Deleted";
				$row["c_lname"] = "Customer";
			}
			if(empty($row["service_id"])){
				$row["service_id"] = "";
				$row["ser_name"] = "Deleted";
				$row["price"] = "";
				$row["time_req"] = "";
			}

			array_push($cusBook,array(
				"booking_id"=>$row["booking_id"],
				"completed"=>boolval($row["completed"]),
				"booked_on"=>$row["booked_on"],
				"starting_time"=>$row["starting_time"],
				"customer_id"=>$row["customer_id"],
				"customer_name"=>$row["c_fname"]." ".$row["c_lname"],
				"staff_id"=>$row["staff_id"],
				"staff_name"=>$row["s_fname"]." ".$row["s_lname"],
				"service_id"=>$row["service_id"],
				"price"=>$row["price"],
				"feedback"=> empty($row["feedback"]) ? 0 : intval($row["feedback"]),
				"time_req"=>$row["time_req"],
				"service_name"=>$row["ser_name"]
			));
			
			$num--;
		}
		$this->last_error=null;
		return $cusBook;
	}
	function getAllBook(){
		$data = $this->getAllBookings();
		if($data!==false){
			return json_decode(json_encode($data));
		}
		return false;
	}

	function deleteBooking(){
		if(empty($this->id)){
		  $this->last_error="Booking_id not found";
		  return false;
		}
		$query = "DELETE from {$this->table_name} where booking_id={$this->id}";
	
		$stmt = $this->conn->prepare($query);
		if($stmt->execute()){
			$this->last_error = null;
			return true;
		}
		$this->last_error=$stmt->errorInfo()[2];
		return false;
	}

	function updateBooking(){
		if(empty($this->id)){
			$this->last_error="Booking_id not found";
			return false;
		}
		if(!$this->isAdmin){
			$query_c = "select staff_id from {$this->table_name} where booking_id={$this->id};";
			$stmt_c = $this->conn->prepare($query_c);	
			$stmt_c->execute();

			$row = $stmt_c->fetch(PDO::FETCH_ASSOC);
			if($row["staff_id"] != $this->staff_id){
				$this->last_error = "This staff cannot change this booking";
				return false;
			}
		}

		$query = "UPDATE {$this->table_name} set completed = 1 where booking_id={$this->id}";
		
		$stmt = $this->conn->prepare($query);
		if($stmt->execute()){
			$this->last_error = null;
			return true;
		}
		$this->last_error=$stmt->errorInfo()[2];
		return false;
	}

	function updateFeedback(){
		if(empty($this->id)){
			$this->last_error = "Booking_id not found";
			return false;
		}

		$query_c = "SELECT customer_id FROM {$this->table_name} WHERE booking_id={$this->id};";
		$stmt_c = $this->conn->prepare($query_c);	
		if(!$stmt_c->execute()){
			$this->last_error = $stmt_c->errorInfo()[2];
			return false;
		}

		$row = $stmt_c->fetch(PDO::FETCH_ASSOC);
		if($row["customer_id"] != $this->customer_id){
			$this->last_error = "This customer cannot rate this booking";
			return false;
		}

		$query = "";

		$query_c = "SELECT * FROM Feedbacks WHERE booking_id = {$this->id}";
		$stmt_c = $this->conn->prepare($query_c);
		if(!$stmt_c->execute()){
			$this->last_error = $stmt_c->errorInfo()[2];
			return false;
		}
		
		if($stmt_c->rowCount() === 1){
			$query = "UPDATE Feedbacks SET feedback = {$this->feedback} WHERE booking_id = {$this->id}";
		}else{
			$query = "INSERT INTO Feedbacks VALUES({$this->customer_id},{$this->id},{$this->feedback});";
		}
		$stmt = $this->conn->prepare($query);
		if($stmt->execute()){
			$this->last_error = null;
			return true;
		}
		$this->last_error=$stmt->errorInfo()[2];
		return false;
	}
}
?>