<?php
class Service {
  
  // database connection and table name
  private $conn;
  private $table_name = "Services";
  private $last_error = null;
  
  // object properties
  public $service_id;
  public $start_time;
  public $name;
  public $price;
  public $description;
  public $time_req;
  public $end_time;
  
  // constructor
  public function __construct($db){
    $this->conn = $db;
  }
  
  function create($admin = false){
    
    // insert query
    $query = "INSERT INTO {$this->table_name}
    (name, description, price, time_req )
    VALUES (:name, :description, :price, :time_req)";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);
    
    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->description=htmlspecialchars(strip_tags($this->description));
    $this->price=htmlspecialchars(strip_tags($this->price));
    $this->time_req=htmlspecialchars(strip_tags($this->time_req));
    
    // bind the values
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":description", $this->description);
    $stmt->bindParam(":price", $this->price);
    $stmt->bindParam(":time_req", $this->time_req);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      $this->service_id = $this->conn->lastInsertId();
      return true;
    }
    
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }
  
  function get_last_error() {
    return $this->last_error;
  }
  
  // check if a service name already exists
  function nameExists() {
    $query = "SELECT service_id FROM {$this->table_name} WHERE name = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->name=htmlspecialchars(strip_tags($this->name));
    $stmt->bindParam(1, $this->name);
    
    // execute the query
    if(!$stmt->execute()){
      $this->last_error = $stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();
    
    $this->last_error=null;
    return ($num>0);
  }
  
  function fetchThisData() {
    $query = "SELECT service_id, name, description, price, time_req FROM {$this->table_name} WHERE service_id = ? LIMIT 1";
    
    $stmt = $this->conn->prepare( $query );
    
    $this->service_id=htmlspecialchars(strip_tags($this->service_id));
    $stmt->bindParam(1, $this->service_id);
    
    // execute the query
    if(!$stmt->execute()){
      $this->last_error = $stmt->errorInfo()[2];
      return false;
    }
    $num = $stmt->rowCount();
    
    if($num>0){
      
      // get record details / values
      $row = $stmt->fetch(PDO::FETCH_ASSOC);
      
      // assign values to object properties
      $this->service_id = $row["service_id"];
      $this->name = $row["name"];
      $this->description = $row["description"];
      $this->price = $row["price"];
      $this->time_req = $row["time_req"];
    }

    $this->last_error = null;
    return true;
  }
  
  function getThisData() {
    if ( (!empty($this->name) && !empty($this->description) && !empty($this->price) && !empty($this->time_req) ) || $this->fetchThisData()) {
      return json_decode(json_encode(array(
        "service_id" => $this->service_id,
        "name" => $this->name,
        "description" => $this->description,
        "price" => $this->price,
        "time_req" => $this->time_req,
      )));
    }
    return false;
  }
  
  // TODO: Update with experiences
  function getThisDataWithExperience() {
    if ($service = $this->getThisData()) {
      $query_staffs = "SELECT staff_id, fname, mname, lname FROM Staffs WHERE role = 'staff'";
      $stmt = $this->conn->prepare( $query_staffs );

      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
      $num = $stmt->rowCount();

      $staffs = array();
      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
          $name = (empty($mname))? "$fname $lname": "$fname $mname $lname";
          $staffs[$staff_id] = $name;
        }
      }

      $query_experience = "SELECT staff_id, experience FROM Experiences WHERE service_id = ?";
      
      $stmt = $this->conn->prepare($query_experience);

      $stmt->bindParam(1, $this->service_id);
      
      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
       $num = $stmt->rowCount();

      $experiences = array();
      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $row["name"] = $staffs[$row["staff_id"]];
          array_push($experiences, $row);
        }
      }
      $service->experiences = $experiences;
    }
    $this->last_error = null;
    return true;
  }

  function getThisDataWithTimeExperience() {
    if ($service = $this->getThisData()) {
      $query_staffs = "SELECT a.staff_id, fname, mname, lname, experience FROM Staffs a, Experiences e WHERE a.staff_id = e.staff_id and service_id = {$this->service_id} and role = 'staff'";
      $stmt = $this->conn->prepare( $query_staffs );

      $this->end_time = $this->time_req + $this->start_time;

      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
      $num = $stmt->rowCount();

      $exp = array();
      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          extract($row);
          $name = (empty($mname))? "$fname $lname": "$fname $mname $lname";
          array_push($exp,array("staff_id" => $staff_id,"name" => $name,"experience" => $experience,"available" => false));
        }
      }

      $query_experience = "SELECT staff_id, experience FROM Experiences WHERE service_id = ? AND staff_id NOT IN(
        SELECT staff_id FROM Bookings b, Services s WHERE b.service_id = s.service_id AND (({$this->start_time} <= b.starting_time AND b.starting_time < {$this->end_time})
        OR ({$this->start_time} < (b.starting_time + s.time_req) AND (b.starting_time + s.time_req) <= {$this->end_time}) OR ({$this->start_time} >= (b.starting_time) AND {$this->end_time} <= (b.starting_time + s.time_req)) OR 
        ({$this->start_time} > b.starting_time AND {$this->end_time} < (b.starting_time + s.time_req))))";
      
      $stmt = $this->conn->prepare( $query_experience );
      $stmt->bindParam(1, $this->service_id);
      
      // execute the query
      if(!$stmt->execute()){
        $this->last_error = $stmt->errorInfo()[2];
        return false;
      }
       $num = $stmt->rowCount();

      // if services found
      if($num>0) {
        // retrieve our table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          foreach ($exp as $key => $val){
            if($val["staff_id"] == $row["staff_id"]){
              $exp[$key]["available"] = true;
            }
          }
          unset($sta);
        }
      }
      $service->experiences = $exp;
    }
    $this->last_error = null;
    return $service;
  }
  
  function getAllData() {
    $query = "SELECT service_id, name, description, price, time_req FROM {$this->table_name}";
    
    $stmt = $this->conn->prepare( $query );
    
    // execute the query
    if(!$stmt->execute()){
      $this->last_error = $stmt->errorInfo()[2];
      return false;
    }
   $num = $stmt->rowCount();
    
    $services_arr = array();
    
    // if services found
    if($num>0) {
      // retrieve our table contents
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        array_push($services_arr, $row);
      }
    }
    $this->last_error = null;
    return $services_arr;
  }
  function deleteService(){
    if(empty($this->service_id)){
      $this->last_error="Service_id not found";
      return false;
    }

    $query_check = "select booking_id from Bookings where service_id = {$this->service_id} and completed=0";
    $stmt_check = $this->conn->prepare($query_check);

    if(!$stmt_check->execute()){
      $this->last_error = $stmt_check->errorInfo()[2];
      return false;
    }
    $num = $stmt_check->rowCount();

    if($num>0){
      $this->last_error="Booking for this service is available, unable to delete";
      return false;
    }
    $query = "DELETE from {$this->table_name} where service_id={$this->service_id}";

    $stmt = $this->conn->prepare($query);
    if($stmt->execute()){
      $this->last_error = null;
      return true;
  }
	$this->last_error=$stmt->errorInfo()[2];
	return false;
  }
  function updateService(){
    if(empty($this->service_id) || empty($this->name) || empty($this->description) || empty($this->price) || 
      empty($this->time_req)){
        $this->last_error ="Required fields are empty";
        return false;
      }
    
    // insert query
    $query = "UPDATE {$this->table_name} 
      set name = :name,price = :price, description = :description,time_req = :time_req 
      where service_id={$this->service_id}";
    
    // prepare the query
    $stmt = $this->conn->prepare($query);

    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->price=htmlspecialchars(strip_tags($this->price));
    $this->description=htmlspecialchars(strip_tags($this->description));
    $this->time_req=htmlspecialchars(strip_tags($this->time_req));

    // bind the values
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":price", $this->price);
    $stmt->bindParam(":description", $this->description);
    $stmt->bindParam(":time_req", $this->time_req);
    
    // execute the query, also check if query was successful
    if($stmt->execute()){
      $this->last_error = null;
      return true;
    }
    
    $this->last_error = $stmt->errorInfo()[2];
    return false;
  }
}