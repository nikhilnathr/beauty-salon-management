<?php
class Response {

  function send($status_code, $data = "", $success = true, $message = "") {
    http_response_code($status_code);
    $response = array(
      "response" => array( "success" => $success, "status_code" => $status_code, "message" => $message),
      "data" => $data
    );
    echo json_encode($response);
    exit();
  }

  function notFound() {
    $this->send(404, "", false, "Not found");
  }
}