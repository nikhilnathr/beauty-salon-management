<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once(__DIR__."/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET");
header("Access-Control-Allow-Headers:Content-Type,Access-Control-Allow-Headers,Authorization,X-Requested-With");

//including class
require_once(__DIR__."/class/Auth.php");
require_once(__DIR__."/config/Database.php");
require_once(__DIR__."/class/Console.php");
require_once(__DIR__."/class/Response.php");
require_once(__DIR__."/class/Feedback.php");

$res = new Response();
$user = new Auth();
if($_SERVER["REQUEST_METHOD"]=="POST"){

    //check if user is logged
    if($user->checkStatus() || $user->is_customer){
        $res->send(400,"",false,"Customer not logged in");
    }

    $db = new Database();
    $conn = $db->connect();
    $newFeed = new Feedback($conn);

    $data = json_decode(file_get_contents("php://input"));

    if(empty($data->customer_id) || empty($data->service_id) || empty($data->feedback)){
        $res->send(400,"",false,"Required fields are empty");
    }

    if(!is_numeric($data->feedback)){
        $res->send(400,"",false,"Not integer value");
    }

    $newFeed->customer_id = intval($data->customer_id);
    $newFeed->service_id = intval($data->service_id);
    $newFeed->feedback = intval($data->feedback);

    if($newFeed->create()){
        $res->send(200,$newFeed->getServiceFeedback(),true,"Feedback added");
    }
    $res->send(400,"",false,$newFeed->getLastError());

}else if($_SERVER["REQUEST_METHOD"]=="GET"){
    if(count($_GET)==1 && $_GET["service_id"]){
        $db = new Database();
        $newFeed = new Feedback($db->connect());
        $newFeed->service_id=$_GET["service_id"];

        if($user->checkStatus()){
            $data = getServiceFeedback();
        }else{
            $res->send(400,"",false,"User not logged in ");
        }
        if($data !== false){
            $res->send(200,$data,true,"Service Feedback returned");
        }

        $res->send(400,"",false,$newFeed->getLastError());
    }
}else if($_SERVER["REQUEST_METHOD"]=="DELETE"){
    if(count($_GET)==1 && !empty($_GET["feed_id"])){
      $db = new Database();
      $newFeed = new Feedback($db->connect());
  
      $newFeed->customer_id = $_GET["customer_id"];
      $newFeed->service_id = $$_GET["service_id"];
  
      if(!$user->checkStatus() || !$user->is_admin){
        $res->send(400,"",false,"Admin not logged in");
      }
      if($newFeed->deleteServiceFeedback()){
        $res->send(200,"",true,"Successfully Deleted");
      }
      $res->send(400,"",false,$service->get_last_error());
    }
  }else{
    $res->notFound();
}