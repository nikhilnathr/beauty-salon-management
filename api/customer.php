<?php
require_once(__DIR__ . "/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include classes
require_once(__DIR__ . "/class/Response.php");
require_once(__DIR__ . "/class/Customer.php");
require_once(__DIR__ . "/class/Auth.php");
require_once(__DIR__ . "/config/Database.php");

$res = new Response();
$user = new Auth();
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // check if already logged in
  if ($user->checkStatus()) {
    $res->send(400, "", false, "User logged in");
  }
  
  $db = new Database();
  $new_customer = new Customer($db->connect());

  $data = json_decode(file_get_contents("php://input"));

  // check for empty values
  if (empty($data->firstname) || empty($data->lastname) || empty($data->email) || empty($data->age)  || empty($data->gender) 
  || empty($data->phone_no) || empty($data->password)) {
    $res->send(400, "", false, "Required fields are empty");
  }
  
  // sanitize values
  if( filter_var($data->email, FILTER_VALIDATE_EMAIL) != $data->email || !is_numeric($data->age) || strlen($data->phone_no) != 10 || !is_numeric($data->phone_no)
  || !in_array($data->gender, array("m", "f")) ) {
      
    $res->send(400, "", false, "Invalid value given");
  } else if (strlen($data->password) < 6) {
    $res->send(400, "", false, "Minimum 6 characters for password");
  }
  
  // populate with customer data
  $new_customer->firstname = $data->firstname;
  $new_customer->middlename = empty($data->middlename)? "": $data->middlename;
  $new_customer->lastname = $data->lastname;
  $new_customer->email = $data->email;
  $new_customer->age = intval($data->age);
  $new_customer->gender = $data->gender;
  $new_customer->address = empty($data->address)? "": $data->address;
  $new_customer->phone_no = intval($data->phone_no);
  $new_customer->password = $data->password;

  if($new_customer->age < 0){
    $res->send(400, "",false,"Enter a valid AGE");
  }
  
  if($new_customer->emailExists()) {
    $res->send(400, "", false, "Email already exists");
  } else if ($new_customer->phoneExists()) {
    $res->send(400, "", false, "Phone number already exists");
  }
  
  if ($new_customer->create()) {
    $res->send(200, $new_customer->getAllData(), true, "New customer created");
  }

  $res->send(400, "", false, $new_customer->get_last_error());
  
  
} else if ($_SERVER["REQUEST_METHOD"] == "GET"){

  if (count($_GET) == 1 && !empty($_GET["id"])) {
    $db = new Database();
    $customer = new Customer($db->connect());
    $customer->customer_id = $_GET["id"];
    
    if ($user->checkStatus() && (($user->is_customer && $user->customer_id == $customer->customer_id) || $user->is_staff)) {
      $data = $customer->getAllData();
    } else {
      $data = $customer->getPublicData();
    }
    if($data !== false) {    
      $res->send(200, $data);
    }

    $res->send(404, "", false, "Customer not found");

  } else {
    if($user->checkStatus() && $user->is_admin){
      $db = new Database();
      $customer = new Customer($db->connect());

      $data = $customer->getAllCustomerData();
      if($data !== false) {    
        $res->send(200, $data);
      }
    }

    // no parameters specified
    if ($user->checkStatus() && $user->is_customer) {
      $db = new Database();
      $customer = new Customer($db->connect());
      $customer->customer_id = $user->customer_id;
      $data = $customer->getAllData();
      if($data !== false) {
        $res->send(200, $data);
      }
    }

    $res->send(400, "", false, "Invalid request");


  }
} else {
  $res->notFound();
}