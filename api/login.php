<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__ . "/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include classes
require_once(__DIR__ . "/class/Response.php");
require_once(__DIR__ . "/class/Auth.php");
require_once(__DIR__ . "/config/Database.php");

$res = new Response();
$db = new Database();
$conn = $db->connect();
$user = new Auth($conn);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // get the post data
  $data = json_decode(file_get_contents("php://input"));
  
  // check if required values are present
  if(empty($data->email) || empty($data->password) || empty($data->type) || count(get_object_vars($data)) != 3) {
    $res->send(400, "", false, "Required fields are empty");
  }
  
  if(!in_array($data->type, array("customer", "staff"))) {
    $res->send(400, "", false, "Invalid value given");
  }
  
  
  if ($user->checkStatus()) {
    $msg = "Already logged in";
  } else {
    $user->email = $data->email;
    if (!$user->validateCredential($data->password, $data->type)) {
      $res->send(400, "", false, "Invalid username / password");
    }
    $msg = "Successful login";
  }
  
  
  
  if($user->type == "staff") {
    $staff = new Staff($conn);
    $staff->staff_id = $user->staff_id;
    $data = $staff->getAllData();
  } else {
    $customer = new Customer($conn);
    $customer->customer_id = $user->customer_id;
    $data = $customer->getAllData();
  }
  $res->send(200, $data, true, $msg);
} else if ($_SERVER["REQUEST_METHOD"] == "GET") {
  // get self
  if($user->checkStatus() && $user->type == "staff") {
    $staff = new Staff($conn);
    $staff->staff_id = $user->staff_id;
    $data = $staff->getAllData();
  } else if ($user->checkStatus()){
    $customer = new Customer($conn);
    $customer->customer_id = $user->customer_id;
    $data = $customer->getAllData();
  } else {
    $res->send(404, "", false, "Invalid route");
  }
  $res->send(200, $data, true, "");
} else {
  $res->notFound();
}