<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once(__DIR__ . "/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET,PUT");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include classes
require_once(__DIR__ . "/class/Response.php");
require_once(__DIR__ . "/class/Service.php");
require_once(__DIR__ . "/class/Auth.php");
require_once(__DIR__ . "/config/Database.php");

$res = new Response();
$user = new Auth();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  
  // check if already logged in
  if (!$user->checkStatus() || !$user->is_admin) {
    $res->send(401, "", false, "Admin not logged in");
  }
  
  $db = new Database();
  $new_service = new Service($db->connect());
  
  $data = json_decode(file_get_contents("php://input"));
  
  // check for empty values
  if (empty($data->name) || empty($data->price) || empty($data->description) || empty($data->time_req)) {
    $res->send(400, "", false, "Required fields are empty");
  }
  
  // sanitize values
  if( !is_numeric($data->price) || !is_numeric($data->time_req)) {
    $res->send(400, "", false, "Invalid value given");
  }
  
  // populate with service data
  $new_service->name = $data->name;
  $new_service->price = intval($data->price);
  $new_service->description = $data->description;
  $new_service->time_req = intval($data->time_req);
  
  if($new_service->nameExists()) {
    $res->send(400, "", false, "Name already exists");
  }
  
  if ($new_service->create()) {
    $res->send(200, $new_service->getThisData(), true, "New service created");
  }
  
  $res->send(400, "", false, $new_service->get_last_error());
  
  
} else if ($_SERVER["REQUEST_METHOD"] == "GET"){
  
  if (count($_GET) == 1 && !empty($_GET["id"])) {
    $db = new Database();
    $service = new Service($db->connect());
    $service->service_id = $_GET["id"];
    
    if ($user->checkStatus()) {
      $data = $service->getThisDataWithExperience();
    } else {
      $data = $service->getThisData();
    }
    
    if($data !== false) {    
      $res->send(200, $data);
    }
    
    $res->send(400, "", false, $service->get_last_error());
    
  }else if(count($_GET)==2 && !empty($_GET["id"]) && !empty($_GET["time"])){
    $db = new Database();
    $service = new Service($db->connect());
    $service->service_id = $_GET["id"];
    $service->start_time = $_GET["time"];

    if($user->checkStatus()){
      $data = $service->getThisDataWithTimeExperience();
    }else{
      $res->send(400, "", false, "Not logged in");
    }
    if($data !== false) {
      $res->send(200, $data);
    }
    $res->send(400, "", false, $service->get_last_error());
  }else {
    
    // no parameters specified
    // get all services
    $db = new Database();
    $services = new Service($db->connect());
    $data = $services->getAllData();

    if($data !== false) {    
      $res->send(200, $data);
    }
    
    $res->send(400, "", false, $services->get_last_error());
  }
}else if($_SERVER["REQUEST_METHOD"] == "PUT"){
  $db = new Database();
  $service = new Service($db->connect());

  if(!$user->checkStatus() || !$user->is_admin){
    $res->send(400,"",false,"Admin not logged in");
  }

  $data = json_decode(file_get_contents("php://input"));

  if (empty($data->service_id) || empty($data->name) || empty($data->price) || empty($data->description) || empty($data->time_req)) {
    $res->send(400, "", false, "Required fields are empty");
  }

  if( !is_numeric($data->price) || !is_numeric($data->time_req)) {
    $res->send(400, "", false, "Invalid value given");
  }

  $service->service_id = intval($data->service_id);
  $service->name = $data->name;
  $service->price = intval($data->price);
  $service->description = $data->description;
  $service->time_req = intval($data->time_req);

  if($service->updateService()){
    $res->send(200,"",true,"Service Successfully Updated");
  }

  $res->send(400,"",false,$service->get_last_error());

}else if($_SERVER["REQUEST_METHOD"]=="DELETE"){
  if(count($_GET)==1 && !empty($_GET["id"])){
    $db = new Database();
    $service = new Service($db->connect());

    $service->service_id = $_GET["id"];

    if(!$user->checkStatus() || !$user->is_admin){
      $res->send(400,"",false,"Admin not logged in");
    }
    if($service->deleteService()){
      $res->send(200,"",true,"Successfully Deleted");
    }

    $res->send(400,"",false,$service->get_last_error());
  }
} else {
  $res->notFound();
}