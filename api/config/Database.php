<?php
require_once(__DIR__ . "/core.php");
class Database{
    public $conn;
 
    // get the database connection
    public function connect(){ 
        $this->conn = null;
        $KEYS = new Keys();

        try{
            $this->conn = new PDO("mysql:host={$KEYS->DB_HOST};dbname={$KEYS->DB_NAME}", $KEYS->DB_USERNAME, $KEYS->DB_PASSWORD);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
    
}