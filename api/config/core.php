<?php
class Keys {
  
  PRIVATE $KEYS;
  
  function __construct() {
    $this->getConfigFromEnv = false;
    if (file_exists(__DIR__ . "/../.keys")) {
      $this->KEYS = json_decode(file_get_contents(__DIR__ . "/../.keys"));
    } else {
      $this->getConfigFromEnv = true;
    }
  }
  
  public function __get($property) {
    if ($this->getConfigFromEnv && getenv($property)) {
      return getenv($property);
    } else if (!$this->getConfigFromEnv && @property_exists($this->KEYS, $property)) {
      return $this->KEYS->$property;
    } else {
      echo "Fatal error: No $property provided in configuration.\n";
      echo "Exiting\n";
      exit();
    }
  } 
}