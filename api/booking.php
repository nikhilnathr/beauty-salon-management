<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once(__DIR__."/includes/headers.php");

header("Access-Control-Allow-Methods: POST,GET,PUT");
header("Access-Control-Allow-Headers:Content-Type,Access-Control-Allow-Headers,Authorization,X-Requested-With");

//include class
require_once(__DIR__."/class/Auth.php");
require_once(__DIR__."/config/Database.php");
require_once(__DIR__."/class/Console.php");
require_once(__DIR__."/class/Response.php");
require_once(__DIR__."/class/Booking.php");

$res = new Response();
$user = new Auth();

if($_SERVER["REQUEST_METHOD"]=="POST"){

	//check if user is logged in
	if(!$user->checkStatus() || !$user->is_customer){
		$res->send(400,"",false,"Customer not logged in");
	}

	$db = new Database();
	$conn = $db->connect();

	$newBook = new Booking($conn);
	$data = json_decode(file_get_contents("php://input"));

	if($user->customer_id != $data->customer_id){
		$res->send(400,"",false,"This Customer not logged in");
	}

	//check empty values
	if(empty($data->starting_time) || empty($data->customer_id) || empty($data->staff_id) || empty($data->service_id)){
		$res->send(400,"",false,"Required fields are empty");
	}else if(!is_numeric($data->starting_time) || !is_numeric($data->customer_id) || !is_numeric($data->staff_id) || !is_numeric($data->service_id)){
		$res->send(400,"",false,"Invalid input");
	} else if ($data->starting_time < time()) {
		$res->send(400, "", false, "Cannot book for a past date");
	}
	

	//populate with values
	$newBook->start_time = intval($data->starting_time);
	$newBook->customer_id = intval($data->customer_id);
	$newBook->staff_id = intval($data->staff_id);
	$newBook->service_id = intval($data->service_id);

	if($newBook->createBooking()){
		$res->send(200,$newBook->getAllData(),true,"New Booking created");
	}

	$res->send(404,"",false,$newBook->getLastError());

}else if($_SERVER["REQUEST_METHOD"]=="GET"){
	if (count($_GET) == 1 && !empty($_GET["id"])){
		$db = new Database();
		$conn = $db->connect();

		$newBook = new Booking($conn);
		$newBook->id = $_GET["id"];

		$newBook->fetchAllData();

		if($user->checkStatus() && ($user->is_admin || ($user->is_customer && ($user->customer_id == $newBook->customer_id)) 
		|| ($user->is_staff && ($user->staff_id == $newBook->staff_id)))){
			$data = $newBook->getAllData();
		}else{
			$res->send(400,"",false,"Error Occured");
		}
		if($data !== false){
			$res->send(200,$data,true,"Booking Details Returned");
		}
		$res->send(400,"",false,$newBook->getLastError());
	}else if(count($_GET) == 1 && !empty($_GET["customer_id"])){
		$db = new Database();
		$conn = $db->connect();

		$newBook = new Booking($conn);
		$newBook->customer_id = $_GET["customer_id"];

		if($user->checkStatus() && ($user->is_admin || ($user->is_customer && ($user->customer_id == $newBook->customer_id)) 
		|| ($user->is_staff && ($user->staff_id == $newBook->staff_id)))){
			$data = $newBook->getCustomerBookData();
		}else{
			$res->send(400,"",false,"Error Occured");
		}
		if($data !== false){
			$res->send(200,$data,true,"Customer Booking Returned");
		}

		$res->send(400,"",false,$newBook->getLastError());
	}else if(count($_GET) == 1 && !empty($_GET["staff_id"])){
		$db = new Database();
		$conn = $db->connect();

		$newBook = new Booking($conn);
		$newBook->staff_id = $_GET["staff_id"];

		if($user->checkStatus() && ($user->is_admin || ($user->is_staff && ($user->staff_id == $newBook->staff_id)))){
			$data = $newBook->getStaffBookData();
		}else{
			$res->send(400,"",false,"Error Occured");
		}
		if($data !== false){
			$res->send(200,$data,true,"Staff Booking Returned");
		}

		$res->send(400,"",false,$newBook->getLastError());
	}else{
		//no parameter
		$db = new Database();
		$conn = $db->connect();
		
		$newBook = new Booking($conn);
		$data = false;

		if($user->checkStatus() && $user->is_admin){
			$data = $newBook->getAllBook();
		}else if($user->checkStatus() && $user->is_staff){
			$newBook->staff_id = $user->staff_id;
			$data = $newBook->getStaffBookData();
			
		}else if($user->checkStatus() && $user->is_customer){
			$newBook->customer_id = $user->customer_id;
			$data = $newBook->getCustomerBookData();
		}else{
			$res->send(400,"",false,"Admin not logged in");
		}

		if($data!==false){
			$res->send(200,$data,true,"Bookings returned");
		}

		$res->send(400,"",false,$newBook->getLastError());
	}
}else if($_SERVER["REQUEST_METHOD"]=="PUT"){

	if(count($_GET) == 1 && $_GET["type"]==="completed"){
		if(!$user->checkStatus() || !$user->is_staff){
			$res->send(400,"",false,"Staff not logged in");
		}
		$db = new Database();
		$conn = $db->connect();

		$newBook = new Booking($conn);
		if($user->is_admin){
			$newBook->isAdmin=true;
		}
		$data = json_decode(file_get_contents("php://input"));

		if(empty($data->booking_id) || !is_numeric($data->booking_id)){
			$res->send(400,"",false,"An integer Booking Id not found");
		}
		$newBook->id = intval($data->booking_id);
		$newBook->staff_id = $user->staff_id;

		if($newBook->updateBooking()){
			$res->send(200,"",true,"Successfully updated");
		}
		$res->send(400,"",false,$newBook->getLastError());
	}else if(count($_GET) == 1 && $_GET["type"] = "rate"){
		if(!$user->checkStatus() || !$user->is_customer){
			$res->send(400,"",false,"Customer not logged in");
		}
		$db = new Database();
		$conn = $db->connect();

		$newBook = new Booking($conn);

		$data = json_decode(file_get_contents("php://input"));

		if(empty($data->feedback) || !is_numeric($data->feedback)){
			$res->send(400,"",false,"An integer feedback not found");
		}
		$newBook->feedback = intval($data->feedback);
		echo($newBook->feedback);

		if($newBook->feedback > 5 || $newBook->feedback < 1){
			$res->send(400,"",false,"Feedback should be in the range 1-5");
		}

		$newBook->id = intval($data->booking_id);
		$newBook->customer_id = $user->customer_id;

		if($newBook->updateFeedback()){
			$res->send(200,"",true,"Successfully updated");
		}
		$res->send(400,"",false,$newBook->getLastError());
	}
}else{
	$res->notFound();
}