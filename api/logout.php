<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once(__DIR__ . "/includes/headers.php");

header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Headers: access");

// include classes
require_once(__DIR__ . "/class/Response.php");
require_once(__DIR__ . "/class/Auth.php");
require_once(__DIR__ . "/config/Database.php");

$res = new Response();

// send 404 if not GET
if ($_SERVER["REQUEST_METHOD"] != "GET") {
  $res->notFound();
}

$user = new Auth();
$user->logout();

$res->send(200, "", true, "Successfully logged out");